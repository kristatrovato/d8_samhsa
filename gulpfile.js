'use strict';

// Theme and local dev variables
//---------------------------------------------
var themepath = 'web/themes/omega_samhsa_main/';  //The Drupal theme path
var src = themepath+'src/';  //The path to the src files (probably won't need to change)
var localhost = "http://d8main.lndo.site";  //The URL for local development



// Gulp plugins
//---------------------------------------------
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var cssnano = require('gulp-cssnano');
var importer = require('node-sass-globbing');
var plumber = require('gulp-plumber');
var uglify = require('gulp-uglify');
var spritesmith = require('gulp.spritesmith');
var imagemin = require('gulp-imagemin');
var buffer = require('vinyl-buffer');
var merge = require('merge-stream');

var imageResize = require('gulp-image-resize');
var rename = require("gulp-rename");

var sass_config = {
  importer: importer,
  includePaths: [
    'node_modules/breakpoint-sass/stylesheets/',
    'node_modules/singularitygs/stylesheets/',
    'node_modules/modularscale-sass/stylesheets',
    //'node_modules/Singularity-extras/stylesheets/',
    'node_modules/compass-mixins/lib/'
  ]
};



// Create the sprite.
// Important: Source images must not have leading numbers (e.g. 2.png -> sprite-2.png)!!!
// SASS does not support the variables that start with numbers!!!
//---------------------------------------------
gulp.task('optimize-sprites', function () {
  var spriteData = gulp.src(src+'images/sprites/**/*.png').pipe(spritesmith({
    imgName: 'sprite.png',
    imgPath: '../../images/sprite.png',
    cssName: '_base_class_sprite.scss',
    cssFormat: 'scss'
  }));

  // Pipe image stream through image optimizer and onto disk
  var imgStream = spriteData.img
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest(themepath+'images'));

  // Create the scss partial
  var cssStream = spriteData.css
    .pipe(gulp.dest(src+'scss/base/class'));

  // Compile our base.css file (this is a version of our sass task but specifically for base.scss)
  var compileScss = spriteData.css
    .pipe(gulp.src(src+'scss/base/base.scss'))
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass(sass_config).on('error', sass.logError))
    .pipe(sourcemaps.write({includeContent: true}))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(autoprefixer({
      browsers: ['last 2 version']
    }))
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(themepath+'css/base'));

  // Return a merged stream to complete all of our events (create image, create scss partial, compile the css)
  return merge(imgStream, cssStream, compileScss);
});



// Optimize images
// Feel free to modify the compression settings
// or add additional compression modules
//---------------------------------------------
gulp.task('optimize-images', () =>
  gulp.src(src+'images/images/**/*')
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({
        plugins: [
          {removeViewBox: true},
          {cleanupIDs: false}
        ]
      })
    ]))
    .pipe(gulp.dest(themepath+'images'))
);


// Optimize Logos
// Need a separate task because these go in the root folder
//---------------------------------------------
gulp.task('optimize-logos', () =>
  gulp.src(src+'images/logos/**/*')
    .pipe(imagemin([
      imagemin.gifsicle({interlaced: true}),
      imagemin.jpegtran({progressive: true}),
      imagemin.optipng({optimizationLevel: 5}),
      imagemin.svgo({
        plugins: [
          {removeViewBox: true},
          {cleanupIDs: false}
        ]
      })
    ]))
    .pipe(gulp.dest(themepath))
);



// Create the appletouch and other device icons
//---------------------------------------------
var resizeImageTasks = [];

[57,76,120,128,152,167,180,192].forEach(function(size) {
  var resizeImageTask = 'resize_' + size;
  gulp.task(resizeImageTask, function() {
    gulp.src(src+'images/appletouch/appletouch.png')
      .pipe(imageResize({
        width:  size,
        height: size,
        upscale: false,
        imageMagick: true,
      }))
      .pipe(imagemin([
        imagemin.optipng({optimizationLevel: 5})
      ]))
      .pipe(rename(function (path) { path.basename += '-' + size; }))
      .pipe(gulp.dest(themepath+'images/appletouch/'));
  });
  resizeImageTasks.push(resizeImageTask);
});

gulp.task('optimize-icons', resizeImageTasks);



// Compress javascript.
//---------------------------------------------
gulp.task('uglify', function() {
  return gulp.src(src+'js/*.js')
    .pipe(uglify())
    .pipe(gulp.dest(themepath+'js'));
});



// Compile sass.
// Sourcemaps is very particular when it comes to other plugins
// See: https://github.com/gulp-sourcemaps/gulp-sourcemaps/wiki/Plugins-with-gulp-sourcemaps-support
//---------------------------------------------
gulp.task('sass', function () {
  return gulp.src(src+'scss/**/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass(sass_config).on('error', sass.logError))
    .pipe(sourcemaps.write({includeContent: true}))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(autoprefixer({
      browsers: ['last 2 version']
    }))
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(themepath+'css'));
});



// Watch for changes and run the functions
//---------------------------------------------
gulp.task('watch', function() {

  //Reload browser
  browserSync.init({
    proxy: localhost
  });

  //Items to watch
  gulp.watch(src+'scss/**/*.scss', ['sass']);
  gulp.watch(src+'js/*.js', ['uglify']);

  //BrowserSync - looks in the output folder (in case there are errors)
  gulp.watch([ themepath+'css/**/*.css', themepath+'js/**/*.js' ]).on('change', browserSync.reload);
});



// Run the tasks
//---------------------------------------------
gulp.task('default', ['watch']);
