(function ($, Drupal) {
    Drupal.behaviors.mindbreezeRadios = {
        attach: function(context, settings) {
        $("input[name='facet']").click(function(){
            var facet1 = $("#facet1").val();
            var facet2 = $("#facet2").val();
            if($("input[name='facet']:checked").val() == facet1) {
                $("#q").attr("placeholder", $("#ph1").val());
            } else {
                $("#q").attr("placeholder", $("#ph2").val());
                 }
        });
}}
})(jQuery, Drupal);