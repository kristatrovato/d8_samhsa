<?phpy

namespace Drupal\samhsa_mindbreeze\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Header Search Block controller.
 */
class HeaderSearch extends ControllerBase {

  /**
   * Returns a render-able array for a search results page.
   */
  public function content() {
    $build = [
      '#cache' => array('max-age' => 0),
    ];
    return $build;
  }

}