<?php

namespace Drupal\samhsa_mindbreeze\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Search Results controller.
 */
class SearchResultsBlock extends ControllerBase {

  /**
   * Returns a render-able array for a search results page.
   */
  public function content() {
    $build = [
      '#markup' => t(''),
      '#theme' => 'search_results',
     '#gfacet' =>  \Drupal::request()->query->get('facet'),
      '#gname' =>   \Drupal::request()->query->get('name'),
      '#gk' =>  \Drupal::request()->query->get('k'),
    ];
    return $build;
  }

}