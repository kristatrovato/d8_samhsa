<?php

namespace Drupal\samhsa_mindbreeze\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Search Results controller.
 */
class SearchResults extends ControllerBase {

  /**
   * Returns a render-able array for a search results page.
   */
  public function content() {
    $build = [
      '#markup' => t(''),
    ];
    return $build;
  }

}