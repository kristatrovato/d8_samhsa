<?php

namespace Drupal\samhsa_mindbreeze\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\UncacheableDependencyTrait;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "header_search",
 *   admin_label = @Translation("Header Search"),
 *   category = "SAMHSA Mindbreeze",
 * )
 */
class HeaderSearch extends BlockBase {
    use UncacheableDependencyTrait;
  /**
   * {@inheritdoc}
   */
  public function build() {
    \Drupal::service('page_cache_kill_switch')->trigger();

    return [
      '#theme' => 'header_search',
      '#title' => 'Header Search',
      '#cache' => ['contexts' => ['url.path', 'url.query_args']],
      '#form_action' => $this->configuration['form_action'],
      '#use_search_icon' => $this->configuration['use_search_icon'],
      '#search_radio' => $this->configuration['search_radio'],
      '#search_label_1' => $this->configuration['search_label_1'],
      '#search_label_2' => $this->configuration['search_label_2'],
      '#search_facet_1' => $this->configuration['search_facet_1'],
      '#search_facet_2' => $this->configuration['search_facet_2'],
      '#search_placeholder_text_1' => $this->configuration['search_placeholder_text_1'],
      '#search_placeholder_text_2' => $this->configuration['search_placeholder_text_2'],
      '#gk' =>  \Drupal::request()->query->get('k'),
      '#gfacet' =>  \Drupal::request()->query->get('facet'),
      ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['form_action'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Form Action (Search Engine)'),
      '#description' => $this->t('When user submits search form, the data will send to this URL.'),
      '#default_value' => isset($config['form_action']) ? $config['form_action'] : 'http://search.samhsa.gov/search?',
      '#attached' => array(
          'library' =>  array(
              'samhsa_mindbreeze/mindbreeze_config')),
    );

    $form['search_radio'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable radio buttons'),
      '#description' => $this->t('Gives the user the option to change the target search to all or only this site'),
      '#default_value' => isset($config['search_radio']) ? $config['search_radio'] : false,
    );


    $form['fs1'] = array(
      '#type' => 'fieldset',
      '#states' => array(
          'visible' => array(
              ':input[data-drupal-selector="edit-settings-search-radio"]' => array(
                  'checked' => true,
              )
          )
      )
    );
    $form['fs1']['radio_1_header'] = array(
        '#type' => 'markup',
        '#markup' => $this->t('<h3 class="mindbreeze-radio-button-header">Radio Button 1 Fields</h3>'),
    );

    $form['search_facet_1'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Facet'),
      '#required' => true,
      '#description' => $config['search_radio'] == true ? $this->t('The target of the search for the first radio button') : $this->t('The target of the search.'),
      '#default_value' => isset($config['search_facet_1']) ? $config['search_facet_1'] : '',
    );

    $form['search_placeholder_text_1'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder Text'),
        '#description' => $config['search_radio'] == true ? $this->t('Text that initially appears in the search box for radio button 1.') : $this->t('Text that initially appears in the search box.'),
        '#default_value' => isset($config['search_placeholder_text_1']) ? $config['search_placeholder_text_1'] : '',
    );

    $form['search_label_1'] = array(
    '#type' => 'textfield',
    '#title' => $this->t('Radio button label'),
    '#default_value' => isset($config['search_label_1']) ? $config['search_label_1'] : '',
        '#states' => array(
            'visible' => array(
                ':input[data-drupal-selector="edit-settings-search-radio"]' => array(
                    'checked' => true,
                )
            )
        )
    );

    $form['fs2'] = array(
        '#type' => 'fieldset',
        '#states' => array(
            'visible' => array(
                ':input[data-drupal-selector="edit-settings-search-radio"]' => array(
                    'checked' => true,
                )
            )
        )
    );

    $form['fs2']['radio_2_header'] = array(
        '#type' => 'markup',
        '#markup' => $this->t('<h3>Radio Button 2 Fields</h3>'),
    );

    $form['search_label_2'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Radio button label'),
        '#default_value' => isset($config['search_label_2']) ? $config['search_label_2'] : '',
        '#states' => array(
            'visible' => array(
                ':input[data-drupal-selector="edit-settings-search-radio"]' => array(
                    'checked' => true,
                )
            )
        )
    );

    $form['search_facet_2'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Facet'),
        '#description' => $this->t('The target of the search for the second radio button.'),
        '#default_value' => isset($config['search_facet_2']) ? $config['search_facet_2'] : '',
        '#states' => array(
            'visible' => array(
                ':input[data-drupal-selector="edit-settings-search-radio"]' => array(
                    'checked' => true,
                )
            )
        )
    );

    $form['search_placeholder_text_2'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Placeholder Text'),
        '#description' => $this->t('Text that initially appears in the search box when radio button 2 is selected.'),
        '#default_value' => isset($config['search_placeholder_text_2']) ? $config['search_placeholder_text_2'] : '',
        '#states' => [
            'visible' => [
                ':input[data-drupal-selector="edit-settings-search-radio"]' => ['checked' => true],
            ],
        ]
    );

    $form['use_search_icon'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('Use Search Icon'),
        '#description' => $this->t('Use search icon instead of the text "Search"'),
        '#default_value' => isset($config['use_search_icon']) ? $config['use_search_icon'] : true,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['form_action'] = $form_state->getValue('form_action');
    $this->configuration['use_search_icon'] = $form_state->getValue('use_search_icon');
    $this->configuration['search_radio'] = $form_state->getValue('search_radio');
//    $config = $form_state->getValue('search_config');
    $this->configuration['search_label_1'] = $form_state->getValue('search_label_1');
    $this->configuration['search_label_2'] = $form_state->getValue('search_label_2');
    $this->configuration['search_facet_1'] = $form_state->getValue('search_facet_1');
    $this->configuration['search_facet_2'] = $form_state->getValue('search_facet_2');
    $this->configuration['search_placeholder_text_1'] = $form_state->getValue('search_placeholder_text_1');
    $this->configuration['search_placeholder_text_2'] = $form_state->getValue('search_placeholder_text_2');
  }
}
