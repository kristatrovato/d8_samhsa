<?php

namespace Drupal\samhsa_mindbreeze\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SearchResults' block.
 *
 * @Block(
 *  id = "search_results",
 *  admin_label = @Translation("Search Results"),
 * )
 */
class SearchResults extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
//    $build = [];
//    $build['search_results']['#markup'] = 'Implement SearchResults.';
//
//    return $build;
    return [
        '#theme' => 'search_results',
        '#title' => 'Search Results',
        '#cache' => ['contexts' => ['url.path', 'url.query_args']],
        '#gk' =>  \Drupal::request()->query->get('k'),
        '#gfacet' =>  \Drupal::request()->query->get('facet'),
//        '#markup' => 'Implement SearchResults.',
    ];
  }
}
