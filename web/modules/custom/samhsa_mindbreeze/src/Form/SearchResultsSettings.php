<?php

namespace Drupal\samhsa_mindbreeze\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure search settings for this site.
 */
class SearchResultsSettings extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'samhsa_mindbreeze_variables';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'samhsa_mindbreeze.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('samhsa_mindbreeze.settings');

    $form['data_collection'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Collection name'),
      '#description' => $this->t('The "collection" value for the project.  Please note that changing this value will affect search <em>throughout the site</em>.'),
      '#default_value' => $config->get('data_collection'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      // Retrieve the configuration
       $this->configFactory->getEditable('samhsa_mindbreeze.settings')
      // Set the submitted configuration setting
      ->set('data_collection', $form_state->getValue('data_collection'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}