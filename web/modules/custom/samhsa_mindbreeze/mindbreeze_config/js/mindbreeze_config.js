(function($) {
    var fd = $("div#edit-settings-search-facet-1--description");
    var facetText = [
        'The target of the search for the first radio button.',
        'The target of the search.',
    ];
    var pd = $("div#edit-settings-search-placeholder-text-1--description");
    var placeholderText = [
        'Text that initially appears in the search box for radio button 1.',
        'Text that initially appears in the search box.',
    ]
    $("#edit-settings-search-radio").change(function(){
        if($("#edit-settings-search-radio:checked").prop("checked")) {
            fd.html(facetText[0]);
            pd.html(placeholderText[0]);
         } else {
            fd.html(facetText[1]);
            pd.html(placeholderText[1]);
         }
     });
})(jQuery);