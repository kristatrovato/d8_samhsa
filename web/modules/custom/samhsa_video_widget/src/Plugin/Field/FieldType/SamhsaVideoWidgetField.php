<?php
namespace Drupal\samhsa_video_widget\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileItem;


/**
 * @FieldType(
 *   id = "samhsa_video_widget",
 *   module = "samhsa_video_widget",
 *   label = @Translation("SAMHSA Video Widget"),
 *   description = @Translation("This field stores SAMHSA video meta information."),
 *   default_widget = "SamhsaVideoWidgetDefaultWidget",
 *   default_formatter = "SamhsaVideoWidgetDefaultFormatter",
 * )
 */

class SamhsaVideoWidgetField extends FieldItemBase {
    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

        $properties = [];

        $properties['video_title'] = DataDefinition::create('string')
            ->setLabel(t('Video Title'))
            ->setDescription(t('Optional title to be displayed instead of the node title.'));

        $properties['video_description'] = DataDefinition::create('string')
            ->setLabel(t('Video Description'))
            ->setDescription(t('Description to be displayed for the video.'));

        $properties['video_length'] = DataDefinition::create('string')
            ->setLabel(t('Video Length'))
            ->setDescription(t('The duration of the video, in hours:minutes:seconds.'));

        $properties['original_filename'] = DataDefinition::create('string')
            ->setLabel(t('Original Filename'))
            ->setDescription(t('The name of the video file that was uploaded to YouTube.'));

        $properties['original_format'] = DataDefinition::create('string')
            ->setLabel(t('Original Format'))
            ->setDescription(t('The video format of the original file that was uploaded to YouTube.'));

        $properties['youtube_url'] = DataDefinition::create('string')
            ->setLabel(t('Video YouTube URL'))
            ->setDescription(t('The YouTube URL at which the video can be referenced.'));

        $properties['mp4_url'] = DataDefinition::create('string')
            ->setLabel(t('Video MP4 URL'))
            ->setDescription(t('The URL at which the MP4 video can be referenced.'));

        $properties['webm_url'] = DataDefinition::create('string')
            ->setLabel(t('Video WebM URL'))
            ->setDescription(t('The URL at which the WebM video can be referenced.'));

        $properties['poster_image_en'] = DataDefinition::create('integer')
            ->setLabel(t('English Poster Image'))
            ->setDescription(t('The image to be displayed in the video player prior to starting the video.'));

        $properties['poster_image_es'] = DataDefinition::create('integer')
            ->setLabel(t('Spanish Poster Image'))
            ->setDescription(t('The image to be displayed in the video player prior to starting the video.'));

        /**
         * The poster image alt text field is commented out because at this time there is no HTML/browser support for
         * alt text for a poster image.
         */
//        $properties['poster_image_alt_text'] = DataDefinition::create('string')
//            ->setLabel(t('Poster Image Alt Text'))
//            ->setDescription(t('The text with which to represent the image when it is not viewable.'));

        $properties['caption_file_en'] = DataDefinition::create('integer')
            ->setLabel(t('English Caption File'))
            ->setDescription(t('The Video Track Text file providing video captions.'));

        $properties['caption_file_es'] = DataDefinition::create('integer')
            ->setLabel(t('Spanish Caption File'))
            ->setDescription(t('The Video Track Text file providing video captions.'));

        $properties['mp3_audio_title'] = DataDefinition::create('string')
            ->setLabel(t('MP3 Audio Title'))
            ->setDescription(t('Optional title to be displayed instead of the node title.'));

        $properties['mp3_audio_file'] = DataDefinition::create('string')
            ->setLabel(t('MP3 Audio File'))
            ->setDescription(t('The MP3 audio file URL.'));

        $properties['mp3_audio_description'] = DataDefinition::create('string')
            ->setLabel(t('MP3 Audio Description'))
            ->setDescription(t('Description to be displayed for the audio.'));

        $properties['mp3_audio_length'] = DataDefinition::create('string')
            ->setLabel(t('MP3 Audio Length'))
            ->setDescription(t('The duration of the audio, in hours:minutes:seconds.'));

        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        $columns = array(
            'video_title' => array(
                'description' => 'Optional title to be displayed instead of the node title.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'video_description' => array(
                'description' => 'Description to be displayed for the video.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'video_length' => array(
                'description' => 'The duration of the video, in hours:minutes:seconds.',
                'type' => 'varchar',
                'length' => 8,
            ),
            'original_filename' => array(
                'description' => 'The name of the video file that was uploaded to YouTube.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'original_format' => array(
                'description' => 'The video format of the original file uploaded to YouTube.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'youtube_url' => array(
                'description' => 'The URL at which the video can be referenced on YouTube.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'mp4_url' => array(
                'description' => 'The URL at which the MP4 video can be referenced.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'webm_url' => array(
                'description' => 'The URL at which the WebM video can be referenced.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'poster_image_en' => array(
                'description' => 'An image to be displayed in the video player prior to the video starting.',
                'type' => 'int',
            ),
            'poster_image_es' => array(
                'description' => 'An image to be displayed in the video player prior to the video starting.',
                'type' => 'int',
            ),
            /**
             * The poster image alt text field is commented out because at this time there is no HTML/browser support for
             * alt text for a poster image.
             */
//            'poster_image_alt_text' => array(
//                'description' => 'Poster image alternate text.',
//                'type' => 'varchar',
//                'length' => 255,
//            ),
            'caption_file_en' => array(
                'description' => 'The Video Track Text file providing video captions.',
                'type' => 'int',
            ),
            'caption_file_es' => array(
                'description' => 'The Video Track Text file providing video captions.',
                'type' => 'int',
            ),
            'mp3_audio_title' => array(
                'description' => 'Optional title to be displayed instead of the node title.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'mp3_audio_description' => array(
                'description' => 'Description to be displayed for the audio.',
                'type' => 'varchar',
                'length' => 255,
            ),
            'mp3_audio_length' => array(
                'description' => 'The duration of the mp3 audio file, in hours:minutes:seconds.',
                'type' => 'varchar',
                'length' => 8,
            ),
            'mp3_audio_file' => array(
                'description' => 'The MP3 audio file URL.',
                'type' => 'varchar',
                'length' => 255,
            ),
        );

        $schema = array(
            'columns' => $columns,
            'indexes' => [],
            //'foreign keys' => array(),
        );

        return $schema;
    }

    /**
     * Define when the field type is empty.
     */
    public function isEmpty() {

        // The field will not be considered empty if the youtube url, mp4 url or mpe audio file properties has a value
        return empty($this->values['youtube_url']) &&
               empty($this->values['mp3_audio_file']) &&
               empty($this->values['mp4_url']);
    }

// Uncomment the following and bring it up to date if the field is to be used as an entity base field
/*     public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
        // Some fields above.
        
        $fields['samhsa_video'] = BaseFieldDefinition::create('samhsa_video')
        ->setLabel(t('SAMHSA Video'))
        ->setDescription(t('Specify meta data for a SAMHSA video.'))
        ->setCardinality(-1); // Ensures that you can have more than just one member
        
        return $fields;
    }
 */    
}
