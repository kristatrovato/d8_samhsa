<?php
namespace Drupal\samhsa_video_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'SamhsaVideoWidgetDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "SamhsaVideoWidgetDefaultWidget",
 *   label = @Translation("SAMHSA Video Widget Default Widget"),
 *   description = @Translation("SAMHSA Video Widget Default Widget"),
 *   field_types = {
 *     "samhsa_video_widget",
 *   }
 * )
 */
//*   multiple_values = TRUE <- Intuitively this should be in the annotation, but it acts opposite of what is expected,


class SamhsaVideoWidgetDefaultWidget extends WidgetBase
{
    /**
     * {@inheritdoc}
     */
    public function formElement(
        FieldItemListInterface $items,
        $delta,
        array $element,
        array &$form,
        FormStateInterface $form_state
    )
    {
        $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();

        // Comment out the following lines if the instance fieldsets should not be collapsible
        $element['#type'] = 'details';
        $element['#open'] = true;

        $element['video_title'] = [
            '#type' => 'textfield',
            '#title' => t('Video Title'),
            '#default_value' => isset($items[$delta]->video_title) ? $items[$delta]->video_title : NULL,
            '#size' => 60,
        ];

        $element['video_description'] = [
            '#type' => 'textarea',
            '#title' => t('Video Description'),
            '#default_value' => isset($items[$delta]->video_description) ? $items[$delta]->video_description : NULL,
            '#size' => 60,
        ];

        $element['video_length'] = [
            '#type' => 'textfield',
            '#title' => t('Video Length'),
            '#default_value' => isset($items[$delta]->video_length) ? $items[$delta]->video_length : NULL,
            '#placeholder' => t('h:mm:ss'),
            '#size' => 8,
            '#element_validate' => array(
                array($this, 'validate_length'),
            ),
        ];

        $element['youtube_url'] = [
            '#type' => 'url',
            '#title' => t('YouTube URL'),
            '#default_value' => isset($items[$delta]->youtube_url) ? $items[$delta]->youtube_url : NULL,
            '#size' => 60,
        ];

        if (in_array($lang, [ 'en', 'und' ])) {
            $element['poster_image_en'] = [
                '#type' => 'managed_file',
                '#title' => t('English Poster Image'),
                '#multiple' => false,
                '#description' => t('The poster image file type must be one of jpg, gif, png or jpeg.'),
                '#default_value' => isset($items[$delta]->poster_image_en) ? [$items[$delta]->poster_image_en] : NULL,
                '#upload_location' => 'public://images/',
                '#upload_validators' => [
                    'file_validate_extensions' => ['jpg gif png jpeg'],
                ],
            ];
        }
        if ($lang == 'es') {
            $element['poster_image_es'] = [
                '#type' => 'managed_file',
                '#title' => t('Spanish Poster Image'),
                '#multiple' => false,
                '#description' => t('The poster image file type must be one of jpg, gif, png or jpeg.'),
                '#default_value' => isset($items[$delta]->poster_image_es) ? [$items[$delta]->poster_image_es] : NULL,
                '#upload_location' => 'public://images/',
                '#upload_validators' => [
                    'file_validate_extensions' => ['jpg gif png jpeg'],
                ],
            ];
        }
        /**
         * The poster image alt text field is commented out because at this time there is no HTML/browser support for
         * alt text for a poster image.
         */
//        $element['poster_image_alt_text'] = [
//            '#type' => 'textfield',
//            '#title' => t('Poster Image Alt Text'),
//            '#description' => t('The text with which to represent the image when it is not viewable.'),
//            '#default_value' => isset($items[$delta]->poster_image_alt_text) ? $items[$delta]->poster_image_alt_text : NULL,
//            '#placeholder' => t('alternate text'),
//            '#size' => 60,
//            '#element_validate' => array(
//                array($this, 'validate_poster'),
//            ),
//        ];
        if (in_array($lang, [ 'en', 'und' ])) {
            $element['caption_file_en'] = [
                '#type' => 'managed_file',
                '#title' => t('English Caption File'),
                '#multiple' => false,
                '#description' => t('The caption file type must be .vtt format.'),
                '#default_value' => isset($items[$delta]->caption_file_en) ? [$items[$delta]->caption_file_en] : NULL,
                '#upload_location' => 'public://videos/vtt_captions/',
                '#upload_validators' => [
                    'file_validate_extensions' => ['vtt'],
                ],
            ];
        }

        if ($lang == 'es') {
            $element['caption_file_es'] = [
                '#type' => 'managed_file',
                '#title' => t('Spanish Caption File'),
                '#multiple' => false,
                '#description' => t('The caption file type must be .vtt format.'),
                '#default_value' => isset($items[$delta]->caption_file_es) ? [$items[$delta]->caption_file_es] : NULL,
                '#upload_location' => 'public://videos/vtt_captions/',
                '#upload_validators' => [
                    'file_validate_extensions' => ['vtt'],
                ],
            ];
        }

        $element['mp3_audio_title'] = [
            '#type' => 'textfield',
            '#title' => t('Audio Title'),
            '#default_value' => isset($items[$delta]->mp3_audio_title) ? $items[$delta]->mp3_audio_title : NULL,
            '#size' => 60,
        ];

        $element['mp3_audio_description'] = [
            '#type' => 'textarea',
            '#title' => t('Audio Description'),
            '#default_value' => isset($items[$delta]->mp3_audio_description) ? $items[$delta]->mp3_audio_description : NULL,
            '#size' => 60,
        ];

        $element['mp3_audio_length'] = [
            '#type' => 'textfield',
            '#title' => t('Audio Length'),
            '#default_value' => isset($items[$delta]->mp3_audio_length) ? $items[$delta]->mp3_audio_length : NULL,
            '#placeholder' => t('h:mm:ss'),
            '#size' => 8,
            '#element_validate' => array(
                array($this, 'validate_length'),
            ),
        ];

        $element['mp3_audio_file'] = [
            '#type' => 'url',
            '#title' => t('MP3 Audio File URL'),
            '#default_value' => isset($items[$delta]->mp3_audio_file) ? $items[$delta]->mp3_audio_file : NULL,
            '#size' => 60,
        ];

        $element['mp4_url'] = [
            '#type' => 'url',
            '#title' => t('MP4 URL'),
            '#default_value' => isset($items[$delta]->mp4_url) ? $items[$delta]->mp4_url : NULL,
            '#size' => 60,
        ];

        $element['original_filename'] = [
            '#type' => 'textfield',
            '#title' => t('Original Filename'),
            '#default_value' => isset($items[$delta]->original_filename) ? $items[$delta]->original_filename : NULL,
            '#size' => 60,
            // comment out the following to enable the field
            '#attributes' => array('disabled' => 'disabled'),
        ];

        $element['original_format'] = [
            '#type' => 'textfield',
            '#title' => t('Original Format'),
            '#default_value' => isset($items[$delta]->original_format) ? $items[$delta]->original_format : NULL,
            '#size' => 20,
            // comment out the following to enable the field
            '#attributes' => array('disabled' => 'disabled'),
        ];
        $element['webm_url'] = [
            '#type' => 'url',
            '#title' => t('WebM URL'),
            '#default_value' => isset($items[$delta]->webm_url) ? $items[$delta]->webm_url : NULL,
            '#size' => 60,
            // comment out the following to enable the field
            '#attributes' => array('disabled' => 'disabled'),
        ];

        return $element;
    }

    /**
     * @inheritdoc
     */
    public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {

        foreach ($values as $delta => $value) {
            if (isset($value['poster_image_en']) && is_array($value['poster_image_en'])) {
                $values[$delta]['poster_image_en'] = $value['poster_image_en'][0];
            }
            if (isset($value['poster_image_es']) && is_array($value['poster_image_es'])) {
                $values[$delta]['poster_image_es'] = $value['poster_image_es'][0];
            }
            if (isset($value['caption_file_en']) && is_array($value['caption_file_en'])) {
                $values[$delta]['caption_file_en'] = $value['caption_file_en'][0];
            }
            if (isset($value['caption_file_es']) && is_array($value['caption_file_es'])) {
                $values[$delta]['caption_file_es'] = $value['caption_file_es'][0];
            }
        }

        return parent::massageFormValues($values, $form, $form_state);
    }

/*
 * The following is not currently called because the alt text field is disabled for the poster image (HTML doesn't allow
 *  for one). If that field gets enabled, the validation property should be moved to each of the poster image fields and
 *  two versions of this function should exist, one for each language, with the calling field being the language-specific caption file field
 *  instead of the alt text field (and the logic modified to account for it here) and the field mentioned below being changed
 *  to the alt text field rather than the language-specific caption file field.
 */
    public static function validate_length($element, FormStateInterface $form_state) {
        $value = $element['#value'];
        if (strlen($value) == 0) {
            $form_state->setValueForElement($element, '');
            return;
        }
        if (!preg_match('/^([\d]{1,2}\:)?([\d]{1,2})?\:([\d]{2})$/', $value)) {
            $form_state->setError($element, t("Length, if specified, must be of the format h:mm:ss, mm:ss or m:ss."));
        }
    }

    public static function validate_poster($element, FormStateInterface $form_state) {
        $value = $element['#value'];
        $field_name = $element['#parents'][0];
        $field = $form_state->getValue($field_name)[0]['poster_image'];
        if (empty($value)) {

            // We have the poster alt text field value, but also need the poster image field value.
            // At this point the name of the field that this widget is attached to is unknown,
            //  so we must discover it in order to user that field name to access the poster field
            if (is_array($field) && sizeof($field) != 0) {
                drupal_set_message(t("A poster image was saved with no corresponding alternate text, which violates 508 accessibility rules."), 'warning');
            }
        }
        elseif (!is_array($field) || sizeof($field) == 0) {
            $form_state->setError($element, t("Alternate text should not be provided unless a poster image has as well."));
            return;
        }
    }

}