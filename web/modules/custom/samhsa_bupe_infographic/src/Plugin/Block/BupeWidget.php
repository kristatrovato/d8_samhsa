<?php

namespace Drupal\samhsa_bupe_infographic\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Component\Serialization\Json;

/**
 * Provides a 'BupeWidget' block.
 *
 * @Block(
 *  id = "bupe_widget",
 *  admin_label = @Translation("Bupe widget"),
 * )
 */
class BupeWidget extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
      $uri = "https://buprenorphine.samhsa.gov/api/current-physician-counts.php";
      try {
          $response = \Drupal::httpClient()->get($uri, array('headers' => array('Accept' => 'text/plain')));
          $data = (string) $response->getBody();
          if (empty($data)) { return FALSE; }
      }
      catch (RequestException $e) { return FALSE; }
      $json_response = Json::decode($data);
      
      $cert30             = $json_response['certified_30_total'];
      $cert100            = $json_response['certified_100_total'];
      $cert275            = $json_response['certified_275_total'];
      
      $build = [];
      $build['#attached']['library'][] = 'samhsa_bupe_infographic/Google.Charts';
      $build['#attached']['library'][] = 'samhsa_bupe_infographic/BupeChart';
      $build['#attached']['drupalSettings']['samhsa_bupe_infographic']['BupeChart']['inpNum1']   = $cert30;
      $build['#attached']['drupalSettings']['samhsa_bupe_infographic']['BupeChart']['inpNum2']   = $cert100;
      $build['#attached']['drupalSettings']['samhsa_bupe_infographic']['BupeChart']['inpNum3']   = $cert275;
      $build['#attached']['drupalSettings']['samhsa_bupe_infographic']['BupeChart']['domObj']    = 'bupe_info_graphic';
      $build['#cache']['max-age'] = 0;
      
      return  $build;
  } // End build
} // End class