<?php

/**
 * @file
 * Contains samhsa_latlon_caching_entity.page.inc.
 *
 * Page callback for Cache lat lon entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Cache lat lon entity templates.
 *
 * Default template: samhsa_latlon_caching_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_samhsa_latlon_caching_entity(array &$variables) {
  // Fetch SamhsaLatLonCachingEntity Entity Object.
  $samhsa_latlon_caching_entity = $variables['elements']['#samhsa_latlon_caching_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
