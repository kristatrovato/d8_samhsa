<?php

namespace Drupal\samhsa_latlon_caching\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Cache lat lon entity edit forms.
 *
 * @ingroup samhsa_latlon_caching
 */
class SamhsaLatLonCachingEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;
    $form['latitude']['widget'][0]['value']['#step'] = 'any';
    $form['latitude']['widget'][0]['value']['#min'] = -89;
    $form['latitude']['widget'][0]['value']['#max'] = 89;
    $form['longitude']['widget'][0]['value']['#step'] = 'any';
    $form['longitude']['widget'][0]['value']['#min'] = -179;
    $form['longitude']['widget'][0]['value']['#max'] = 179;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Cache lat lon entity.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Cache lat lon entity.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.samhsa_latlon_caching_entity.canonical', ['samhsa_latlon_caching_entity' => $entity->id()]);
  }

}
