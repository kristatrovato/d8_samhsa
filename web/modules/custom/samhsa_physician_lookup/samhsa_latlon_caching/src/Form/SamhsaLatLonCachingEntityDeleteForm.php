<?php

namespace Drupal\samhsa_latlon_caching\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Cache lat lon entity entities.
 *
 * @ingroup samhsa_latlon_caching
 */
class SamhsaLatLonCachingEntityDeleteForm extends ContentEntityDeleteForm {


}
