<?php

/**
 * @file
 * Contains Drupal\samhsa_latlon_caching\Form\SamhsaLatLonCachingConfigForm.
 */

namespace Drupal\samhsa_latlon_caching\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SamhsaLatLonCachingConfigFormv.
 *
 * @package Drupal\samhsa_latlon_caching\Form
 */
class SamhsaLatLonCachingConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'samhsa_latlon_caching.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'samhsa_latlon_caching_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('samhsa_latlon_caching.settings');

    $form['google_maps'] = array(
      '#type' => 'details',
      '#title' => $this->t("Google Maps API"),
      '#open' => FALSE,
    );
    $form['google_maps']['google_maps_api_url_postal_code'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("API's URL for Postal Code"),
      '#default_value' => $config->get('google_maps_api_url_postal_code'),
      '#description' => $this->t('Use {postal_code} and {country} as 
      placeholders for postal code and country, respectively.'),
    );
    $form['google_maps']['google_maps_api_url_locality'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("API's URL for Locality"),
      '#default_value' => $config->get('google_maps_api_url_locality'),
      '#description' => $this->t('Use {locality}, as a placeholder for locality.'),
    );
    $form['google_maps']['google_maps_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("API's key"),
      '#default_value' => $config->get('google_maps_api_key'),
    );
    $form['google_maps']['google_maps_delay'] = array(
      '#type' => 'number',
      '#title' => $this->t("Delay between requests"),
      '#default_value' => $config->get('google_maps_delay'),
      '#description' => $this->t('Time in microseconds to be delayed between two consecutive requests to Google Maps API.'),
    );

    $form['mapquest'] = array(
      '#type' => 'details',
      '#title' => $this->t("MapQuest API"),
      '#open' => FALSE,
    );
    $form['mapquest']['mapquest_api_url_postal_code'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("API's URL for Postal Code"),
      '#default_value' => $config->get('mapquest_api_url_postal_code'),
      '#description' => $this->t('Use {postal_code} and {country} as 
      placeholders for postal code and country, respectively.'),
    );
    $form['mapquest']['mapquest_api_url_locality'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("API's URL for Locality"),
      '#default_value' => $config->get('mapquest_api_url_locality'),
      '#description' => $this->t('Use {locality}, as a placeholder for locality.'),
    );
    $form['mapquest']['mapquest_api_key'] = array(
      '#type' => 'textfield',
      '#title' => $this->t("API's key"),
      '#default_value' => $config->get('mapquest_api_key'),
    );
    $form['mapquest']['mapquest_delay'] = array(
      '#type' => 'number',
      '#title' => $this->t("Delay between requests"),
      '#default_value' => $config->get('mapquest_delay'),
      '#description' => $this->t('Time in microseconds to be delayed between two consecutive requests to MapQuest API.'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#id'] == 'update-null-latlon') {
      $a = 1;
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('samhsa_latlon_caching.settings')
      ->set('google_maps_api_url_postal_code', $form_state->getValue('google_maps_api_url_postal_code'))
      ->set('google_maps_api_url_locality', $form_state->getValue('google_maps_api_url_locality'))
      ->set('google_maps_api_key', $form_state->getValue('google_maps_api_key'))
      ->set('google_maps_delay', $form_state->getValue('google_maps_delay'))
      ->set('mapquest_api_url_postal_code', $form_state->getValue('mapquest_api_url_postal_code'))
      ->set('mapquest_api_url_locality', $form_state->getValue('mapquest_api_url_locality'))
      ->set('mapquest_api_key', $form_state->getValue('mapquest_api_key'))
      ->set('mapquest_delay', $form_state->getValue('mapquest_delay'))
      ->save();
  }

}
