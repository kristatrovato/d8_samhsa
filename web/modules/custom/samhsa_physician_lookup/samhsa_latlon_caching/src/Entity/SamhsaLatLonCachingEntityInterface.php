<?php

namespace Drupal\samhsa_latlon_caching\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Cache lat lon entity entities.
 *
 * @ingroup samhsa_latlon_caching
 */
interface SamhsaLatLonCachingEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.


  /**
   * Gets the Cache lat lon entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cache lat lon entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Cache lat lon entity creation timestamp.
   *
   * @param int $timestamp
   *   The Cache lat lon entity creation timestamp.
   *
   * @return \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface
   *   The called Cache lat lon entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Hash Key of the latlon Entity.
   *
   * @return string
   *   Hash Key of the location.
   */
  public function getHashKey();

  /**
   * Sets the Hash Key of the latlon Entity.
   *
   * @param string $value
   *   The Hash Key's state.
   *
   * @return \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface
   *   The called Cache lat lon entity entity.
   */
  public function setHashKey($value);

  /**
   * Gets the City of the latlon Entity.
   *
   * @return string
   *   City name of the location.
   */
  public function getCity();

  /**
   * Sets the City of the latlon Entity.
   *
   * @param string $value
   *   The location's city name.
   *
   * @return \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface
   *   The called Cache lat lon entity entity.
   */
  public function setCity($value);

  /**
   * Gets the State of the latlon Entity.
   *
   * @return string
   *   State of the location.
   */
  public function getState();

  /**
   * Sets the State of the latlon Entity.
   *
   * @param string $value
   *   The location's state.
   *
   * @return \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface
   *   The called Cache lat lon entity entity.
   */
  public function setState($value);

  /**
   * Gets the Country of the latlon Entity.
   *
   * @return string
   *   Country of the location.
   */
  public function getCountry();

  /**
   * Sets the Country of the latlon Entity.
   *
   * @param string $value
   *   The location's country.
   *
   * @return \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface
   *   The called Cache lat lon entity entity.
   */
  public function setCountry($value);

  /**
   * Gets the Postal Code of the latlon Entity.
   *
   * @return string
   *   Postal Code of the location.
   */
  public function getPostalCode();

  /**
   * Sets the Postal Code of the latlon Entity.
   *
   * @param string $value
   *   The location's PostalCode.
   *
   * @return \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface
   *   The called Cache lat lon entity entity.
   */
  public function setPostalCode($value);

  /**
   * Gets the Latitude of the latlon Entity.
   *
   * @return string
   *   Latitude of the location.
   */
  public function getLatitude();

  /**
   * Sets the Latitude of the latlon Entity.
   *
   * @param string $value
   *   The location's Latitude.
   *
   * @return \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface
   *   The called Cache lat lon entity entity.
   */
  public function setLatitude($value);


  /**
   * Gets the Longitude of the latlon Entity.
   *
   * @return string
   *   Longitude of the location.
   */
  public function getLongitude();

  /**
   * Sets the Longitude of the latlon Entity.
   *
   * @param string $value
   *   The location's Longitude.
   *
   * @return \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface
   *   The called Cache lat lon entity entity.
   */
  public function setLongitude($value);

}
