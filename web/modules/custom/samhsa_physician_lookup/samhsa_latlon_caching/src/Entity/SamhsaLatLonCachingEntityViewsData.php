<?php

namespace Drupal\samhsa_latlon_caching\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Cache lat lon entity entities.
 */
class SamhsaLatLonCachingEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
