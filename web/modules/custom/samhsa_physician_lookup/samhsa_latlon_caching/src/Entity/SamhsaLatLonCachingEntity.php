<?php

namespace Drupal\samhsa_latlon_caching\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Cache lat lon entity entity.
 *
 * @ingroup samhsa_latlon_caching
 *
 * @ContentEntityType(
 *   id = "samhsa_latlon_caching_entity",
 *   label = @Translation("Cache Latitude and Longitude Entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\samhsa_latlon_caching\SamhsaLatLonCachingEntityListBuilder",
 *     "views_data" = "Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\samhsa_latlon_caching\Form\SamhsaLatLonCachingEntityForm",
 *       "add" = "Drupal\samhsa_latlon_caching\Form\SamhsaLatLonCachingEntityForm",
 *       "edit" = "Drupal\samhsa_latlon_caching\Form\SamhsaLatLonCachingEntityForm",
 *       "delete" = "Drupal\samhsa_latlon_caching\Form\SamhsaLatLonCachingEntityDeleteForm",
 *     },
 *     "access" = "Drupal\samhsa_latlon_caching\SamhsaLatLonCachingEntityAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\samhsa_latlon_caching\SamhsaLatLonCachingEntityHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cache_samhsa_latlon",
 *   admin_permission = "administer samhsa latlon caching entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "city",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/samhsa_latlon_caching_entity/{samhsa_latlon_caching_entity}",
 *     "add-form" = "/admin/structure/samhsa_latlon_caching_entity/add",
 *     "edit-form" = "/admin/structure/samhsa_latlon_caching_entity/{samhsa_latlon_caching_entity}/edit",
 *     "delete-form" = "/admin/structure/samhsa_latlon_caching_entity/{samhsa_latlon_caching_entity}/delete",
 *     "collection" = "/admin/structure/samhsa_latlon_caching_entity",
 *   },
 *   field_ui_base_route = "samhsa_latlon_caching_entity.settings"
 * )
 */
class SamhsaLatLonCachingEntity extends ContentEntityBase implements SamhsaLatLonCachingEntityInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getHashKey() {
    return $this->get('hash_key')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setHashKey($value) {
    $this->set('hash_key', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCity() {
    return $this->get('city')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCity($value) {
    $this->set('city', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setState($value) {
    $this->set('state', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCountry() {
    return $this->get('country')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCountry($value) {
    $this->set('country', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPostalCode() {
    return $this->get('postal_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPostalCode($value) {
    $this->set('postal_code', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLatitude() {
    return $this->get('latitude')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLatitude($value) {
    $this->set('latitude', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLongitude() {
    return $this->get('longitude')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLongitude($value) {
    $this->set('longitude', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Cache Lat Lon entity.'))
      ->setReadOnly(TRUE);
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Cache Lat Lon entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Cache Lat Lon entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['country'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Country'))
      ->setDescription(t('Country.'))
      ->setSettings(array(
        'max_length' => 15,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 10,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['hash_key'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Hash Key'))
      ->setDescription(t('Hash Key.'))
      ->setSettings(array(
        'max_length' => 256,
        'text_processing' => 0,
      ));

    $fields['state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t('State.'))
      ->setSettings(array(
        'max_length' => 40,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 15,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 15,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['city'] = BaseFieldDefinition::create('string')
      ->setLabel(t('City'))
      ->setDescription(t('City.'))
      ->setSettings(array(
        'max_length' => 40,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 20,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 20,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['postal_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Postal Code'))
      ->setDescription(t('Postal Code.'))
      ->setSettings(array(
        'max_length' => 12,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 25,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 25,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['longitude'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Longitude'))
      ->setDescription('Longitude.')
      ->setSettings(array(
        'precision' => 10,
        'scale' => 7,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'decimal',
        'weight' => 35,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 35,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['latitude'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Latitude'))
      ->setDescription('Latitude.')
      ->setSettings(array(
        'precision' => 10,
        'scale' => 7,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'decimal',
        'weight' => 30,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 30,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['request'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Request'))
      ->setDescription(t('Request.'))
      ->setSettings(array(
        'max_length' => 512,
        'text_processing' => 0,
      ))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 40,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 40,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['response'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Response'))
      ->setDescription(t('Response.'))
      ->setDefaultValue('')
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'string',
        'weight' => 45,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => 45,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));


    return $fields;
  }

}
