<?php

namespace Drupal\samhsa_latlon_caching;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Cache lat lon entity entities.
 *
 * @ingroup samhsa_latlon_caching
 */
class SamhsaLatLonCachingEntityListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Cache LatLon Entity ID');
    $header['city'] = $this->t('City');
    $header['state'] = $this->t('State');
    $header['country'] = $this->t('Country');
    $header['postal_code'] = $this->t('Postal Code');
    $header['latitude'] = $this->t('Latitude');
    $header['longitude'] = $this->t('Longitude');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntity */
    $row['id'] = $entity->id();
    $row['city'] = Link::createFromRoute(
      $entity->label(),
      'entity.samhsa_latlon_caching_entity.edit_form',
      ['samhsa_latlon_caching_entity' => $entity->id()]
    );
    $row['state'] = $entity->getState();
    $row['country'] = $entity->getCountry();
    $row['postal_code'] = $entity->getPostalCode();
    $row['latitude'] = $entity->getLatitude();
    $row['longitude'] = $entity->getLongitude();
    return $row + parent::buildRow($entity);
  }

}
