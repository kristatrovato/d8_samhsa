<?php

namespace Drupal\samhsa_latlon_caching;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Cache lat lon entity entity.
 *
 * @see \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntity.
 */
class SamhsaLatLonCachingEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\samhsa_latlon_caching\Entity\SamhsaLatLonCachingEntityInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished samhsa latlon caching entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published samhsa latlon caching entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit samhsa latlon caching entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete samhsa latlon caching entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add samhsa latlon caching entities');
  }

}
