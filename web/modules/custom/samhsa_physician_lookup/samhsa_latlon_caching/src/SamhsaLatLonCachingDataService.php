<?php
/**
 * @file
 * Contains \Drupal\samhsa_latlon_caching\SamhsaLatLonCachingDataService.
 */

namespace Drupal\samhsa_latlon_caching;

use \Drupal\Core\Database\Database;

/**
 * Class SamhsaLatLonCachingDataService.
 *
 * @package Drupal\samhsa_latlon_caching
 */
class SamhsaLatLonCachingDataService {

  private $googleApiUrlPostalCode = NULL;
  private $googleApikey = NULL;
  private $googleRequestsDelay = 0;
  private $mapquestApiUrlPostalCode = NULL;
  private $mapquestApikey = NULL;
  private $mapquestRequestsDelay = 0;

  private $countryAbbreviations = [
    'US' => 'United States',
    'USA' => 'United States',
    'MX' => 'Mexico',
    'MEX' => 'Mexico',
    'CA' => 'Canada',
    'CAN' => 'Canada',
  ];

  private $statesAbbreviations = [
    'united states' => [
      'AL' => 'Alabama',
      'AK' => 'Alaska',
      'AZ' => 'Arizona',
      'AR' => 'Arkansas',
      'CA' => 'California',
      'CO' => 'Colorado',
      'CT' => 'Connecticut',
      'DE' => 'Delaware',
      'DC' => 'District of Columbia',
      'FL' => 'Florida',
      'GA' => 'Georgia',
      'HI' => 'Hawaii',
      'ID' => 'Idaho',
      'IL' => 'Illinois',
      'IN' => 'Indiana',
      'IA' => 'Iowa',
      'KS' => 'Kansas',
      'KY' => 'Kentucky',
      'LA' => 'Louisiana',
      'ME' => 'Maine',
      'MD' => 'Maryland',
      'MA' => 'Massachusetts',
      'MI' => 'Michigan',
      'MN' => 'Minnesota',
      'MS' => 'Mississippi',
      'MO' => 'Missouri',
      'MT' => 'Montana',
      'NE' => 'Nebraska',
      'NV' => 'Nevada',
      'NH' => 'New Hampshire',
      'NJ' => 'New Jersey',
      'NM' => 'New Mexico',
      'NY' => 'New York',
      'NC' => 'North Carolina',
      'ND' => 'North Dakota',
      'OH' => 'Ohio',
      'OK' => 'Oklahoma',
      'OR' => 'Oregon',
      'PA' => 'Pennsylvania',
      'PR' => 'Puerto Rico',
      'RI' => 'Rhode Island',
      'SC' => 'South Carolina',
      'SD' => 'South Dakota',
      'TN' => 'Tennessee',
      'TX' => 'Texas',
      'UT' => 'Utah',
      'VT' => 'Vermont',
      'VA' => 'Virginia',
      'VI' => 'Virgin Islands',
      'WA' => 'Washington',
      'WV' => 'West Virginia',
      'WI' => 'Wisconsin',
      'WY' => 'Wyoming',
      'AE' => 'Armed Forces Africa \ Canada \ Europe \ Middle East',
      'AA' => 'Armed Forces America (Except Canada)',
      'AP' => 'Armed Forces Pacific',
    ],
    'canada' => [
      'AB' => 'Alberta',
      'BC' => 'British Columbia',
      'MB' => 'Manitoba',
      'NB' => 'New Brunswick',
      'NL' => 'Newfoundland and Labrador',
      'NS' => 'Nova Scotia',
      'NT' => 'Northwest Territories',
      'NU' => 'Nunavut',
      'ON' => 'Ontario',
      'PE' => 'Prince Edward Island',
      'QC' => 'Quebec',
      'SK' => 'Saskatchewan',
      'YT' => 'Yukon',
    ],
    'mexico' => [
      'AG' => 'Aguascalientes',
      'BN' => 'Baja California',
      'BS' => 'Baja California Sur',
      'CM' => 'Campeche',
      'CP' => 'Chiapas',
      'CH' => 'Chihuahua',
      'DF' => 'Ciudad de México',
      'CA' => 'Coahuila',
      'CL' => 'Colima',
      'DU' => 'Durango',
      'GJ' => 'Guanajuato',
      'GR' => 'Guerrero',
      'HI' => 'Hidalgo',
      'JA' => 'Jalisco',
      'MX' => 'México',
      'MC' => 'Michoacán',
      'MR' => 'Morelos',
      'NA' => 'Nayarit',
      'NL' => 'Nuevo León',
      'OA' => 'Oaxaca',
      'PU' => 'Puebla',
      'QE' => 'Querétaro',
      'QR' => 'Quintana Roo',
      'SL' => 'San Luis Potosí',
      'SI' => 'Sinaloa',
      'SO' => 'Sonora',
      'TB' => 'Tabasco',
      'TM' => 'Tamaulipas',
      'TL' => 'Tlaxcala',
      'VE' => 'Veracruz',
      'YU' => 'Yucatán',
      'ZA' => 'Zacatecas',
      'AGU' => 'Aguascalientes',
      'BCN' => 'Baja California',
      'BCS' => 'Baja California Sur',
      'CAM' => 'Campeche',
      'CHP' => 'Chiapas',
      'CHH' => 'Chihuahua',
      'DIF' => 'Ciudad de México',
      'COA' => 'Coahuila',
      'COL' => 'Colima',
      'DUR' => 'Durango',
      'GUA' => 'Guanajuato',
      'GRO' => 'Guerrero',
      'HID' => 'Hidalgo',
      'JAL' => 'Jalisco',
      'MEX' => 'México',
      'MIC' => 'Michoacán',
      'MOR' => 'Morelos',
      'NAY' => 'Nayarit',
      'NLE' => 'Nuevo León',
      'OAX' => 'Oaxaca',
      'PUE' => 'Puebla',
      'QUE' => 'Querétaro',
      'ROO' => 'Quintana Roo',
      'SLP' => 'San Luis Potosí',
      'SIN' => 'Sinaloa',
      'SON' => 'Sonora',
      'TAB' => 'Tabasco',
      'TAM' => 'Tamaulipas',
      'TLA' => 'Tlaxcala',
      'VER' => 'Veracruz',
      'YUC' => 'Yucatán',
      'ZAC' => 'Zacatecas',
    ],
  ];

  private $normalizableChars = [
    'Š' => 'S',
    'š' => 's',
    'Ž' => 'Z',
    'ž' => 'z',
    'À' => 'A',
    'Á' => 'A',
    'Â' => 'A',
    'Ã' => 'A',
    'Ä' => 'A',
    'Å' => 'A',
    'Æ' => 'A',
    'Ç' => 'C',
    'È' => 'E',
    'É' => 'E',
    'Ê' => 'E',
    'Ë' => 'E',
    'Ì' => 'I',
    'Í' => 'I',
    'Î' => 'I',
    'Ï' => 'I',
    'Ñ' => 'N',
    'Ò' => 'O',
    'Ó' => 'O',
    'Ô' => 'O',
    'Õ' => 'O',
    'Ö' => 'O',
    'Ø' => 'O',
    'Ù' => 'U',
    'Ú' => 'U',
    'Û' => 'U',
    'Ü' => 'U',
    'Ý' => 'Y',
    'Þ' => 'B',
    'ß' => 'Ss',
    'à' => 'a',
    'á' => 'a',
    'â' => 'a',
    'ã' => 'a',
    'ä' => 'a',
    'å' => 'a',
    'æ' => 'a',
    'ç' => 'c',
    'è' => 'e',
    'é' => 'e',
    'ê' => 'e',
    'ë' => 'e',
    'ì' => 'i',
    'í' => 'i',
    'î' => 'i',
    'ï' => 'i',
    'ð' => 'o',
    'ñ' => 'n',
    'ò' => 'o',
    'ó' => 'o',
    'ô' => 'o',
    'õ' => 'o',
    'ö' => 'o',
    'ø' => 'o',
    'ù' => 'u',
    'ú' => 'u',
    'û' => 'u',
    'ý' => 'y',
    'þ' => 'b',
    'ÿ' => 'y',
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $config = \Drupal::config('samhsa_latlon_caching.settings');
    $this->googleApiUrlPostalCode = $config->get('google_maps_api_url_postal_code');
    $this->googleApiUrlLocality = $config->get('google_maps_api_url_locality');
    $this->googleApikey = $config->get('google_maps_api_key');
    $this->googleRequestsDelay = $config->get('google_maps_delay');
    $this->mapquestApiUrlPostalCode = $config->get('mapquest_api_url_postal_code');
    $this->mapquestApiUrlLocality = $config->get('mapquest_api_url_locality');
    $this->mapquestApikey = $config->get('mapquest_api_key');
    $this->mapquestRequestsDelay = $config->get('mapquest_delay');
    foreach ($this->statesAbbreviations['mexico'] as &$name) {
      $name = strtr($name, $this->normalizableChars);
    }
  }

  /**
   * Get the lat/lon based on postal code and country.
   *
   * @param string $postal_code
   *   Postal Code.
   * @param string $country
   *   Country.
   * @param boolean $batch_processing
   *   Whether the request is part of a batch processing. If true, it delays before next request.
   *
   * @return boolean/object
   *   ::latitude float,
   *   ::longitude float.
   */
  public function geocodingFromPostalCode($postal_code, $country = 'USA', $batch_processing = FALSE) {
    $hash_key = $this->generateHashKey([$postal_code, $country]);
    if (!$result = $this->geocodingFromCache($hash_key)) {
      if ($result = $this->geocodingFromPostalCodeGoogle($postal_code, $country, $batch_processing)) {
        $this->updateLocationCache($result, $hash_key);
      }
      elseif ($result = $this->geocodingFromPostalCodeMapQuest($postal_code, $country, $batch_processing)) {
        $this->updateLocationCache($result, $hash_key);
      }
    }
    if ($result) {
      $result->latlon = $result->latitude . ',' . $result->longitude;
      $result->formattedAddress = $result->city .
        ', ' .
        $result->state .
        chr(32) .
        strtoupper($result->postal_code) .
        ', ' .
        $result->country;
    }
    return $result;
  }

  /**
   * Get the lat/lon based on Google Autosuggestion.
   *
   * @param string $location
   *   Location value provided by Google Autosuggestion.
   * @param boolean $batch_processing
   *   Whether the request is part of a batch processing. If true, it delays before next request.
   *
   * @return object
   *   ::latitude float,
   *   ::longitude float.
   */
  public function geocodingFromAutoSuggestions($location, $batch_processing = FALSE) {
    $hash_key = $this->generateHashKey([$location]);
    if (!$result = $this->geocodingFromCache($hash_key)) {
      if ($result = $this->geocodingFromAutoSuggestionsGoogle($location, $batch_processing)) {
        $this->updateLocationCache($result, $hash_key);
      }
      elseif ($result = $this->geocodingFromAutoSuggestionsMapQuest($location, $batch_processing)) {
        $this->updateLocationCache($result, $hash_key);
      }
    }
    if ($result) {
      $result->latlon = $result->latitude . ',' . $result->longitude;
      if (isset($result->postal_code) && $result->postal_code) {
        $state = $result->state . chr(32) . strtoupper($result->postal_code);
      }
      else {
        $state = $result->state;
      }
      $result->formattedAddress = $result->city .
        ', ' .
        $state .
        ', ' .
        $result->country;
    }
    return $result;
  }

  private function updateLocationCache($geocode, $hash_key) {
    $connection = Database::getConnection();
    $now = time();
    $values = [
      'uuid' => $this->generateUuid(),
      'hash_key' => $hash_key,
      'user_id' => \Drupal::currentUser()->id(),
      'country' => $geocode->country,
      'latitude' => $geocode->latitude,
      'longitude' => $geocode->longitude,
      'city' => $geocode->city ?? NULL,
      'state' => $geocode->state ?? NULL,
      'postal_code' => $geocode->postal_code ?? NULL,
      'request' => $geocode->request,
      'response' => preg_replace('/\s+/S', '', $geocode->response),
      'created' => $now,
      'changed' => $now,
    ];
    $query = $connection->insert('cache_samhsa_latlon');
    $query->fields($values);
    $query->execute();
  }

  private function generateHashKey($values) {
    return hash( 'sha256', serialize($values));
  }

  /**
   * Get the lat/lon from cache based on postal code and country.
   *
   * @param string $hash_key
   *   Key for search.
   *
   * @return object
   *   ::latitude float,
   *   ::longitude float.
   */
  public function geocodingFromCache($hash_key) {
    $connection = Database::getConnection();
    $query = $connection->select('cache_samhsa_latlon', 'c');
    $query->addField('c', 'latitude');
    $query->addField('c', 'longitude');
    $query->addField('c', 'city');
    $query->addField('c', 'state');
    $query->addField('c', 'country');
    $query->addField('c', 'postal_code');
    $query->condition('c.hash_key', $hash_key);
    return $query->execute()->fetchObject();
  }

  /**
   * Get the lat/lon from Google Geocoding API based on Google Autosuggestion.
   *
   * @param string $location
   *   Location value provided by Google Autosuggestion.
   * @param boolean $batch_processing
   *   Whether the request is part of a batch processing. If true, it delays before next request.
   *
   * @return object
   *   ::latitude float,
   *   ::longitude float.
   */
  public function geocodingFromAutoSuggestionsGoogle($location, $batch_processing = FALSE) {
    $result = new \stdClass;
    $request_google = trim($this->googleApiUrlLocality);
    $request_google = str_replace('{locality}', urlencode($location), $request_google);
    $request_google .= '&key=' . $this->googleApikey;
    if ($response = file_get_contents($request_google)) {
      $location_data = json_decode($response, TRUE);
      if ($location_data['status'] == 'OK') {
        $result->latitude = $location_data['results'][0]['geometry']['location']['lat'];
        $result->longitude = $location_data['results'][0]['geometry']['location']['lng'];
        $result->request = $request_google;
        $result->response = $response;
        foreach ($location_data['results'][0]['address_components'] as $address_component) {
          if ($address_component['types'][0] == 'postal_code') {
            $result->postal_code = $address_component['short_name'];
          }
          elseif ($address_component['types'][0] == 'locality') {
            $result->city = $address_component['short_name'];
          }
          elseif ($address_component['types'][0] == 'administrative_area_level_1') {
            $result->state = $address_component['long_name'];
          }
          elseif ($address_component['types'][0] == 'country') {
            $result->country = $address_component['long_name'];
          }
        }
      }
      else {
        $result = FALSE;
      }
      if ($batch_processing) {
        usleep($this->googleRequestsDelay);
      }
    }
    else {
      $result = FALSE;
    }
    if (empty( (array) $result)) {
      return FALSE;
    }
    else {
      return $result;
    };
  }
  
  /**
   * Get the lat/lon from Google Geocoding API based on postal code and country.
   *
   * @param string $postal_code
   *   Postal Code.
   * @param string $country
   *   Country.
   * @param boolean $batch_processing
   *   Whether the request is part of a batch processing. If true, it delays before next request.
   *
   * @return object
   *   ::latitude float,
   *   ::longitude float.
   */
  public function geocodingFromPostalCodeGoogle($postal_code, $country = 'USA', $batch_processing = FALSE) {
    $result = new \stdClass;
    $request_google = trim($this->googleApiUrlPostalCode);
    $request_google = str_replace('{postal_code}', urlencode($postal_code), $request_google);
    $request_google = str_replace('{country}', urlencode($country), $request_google);
    $request_google .= '&key=' . $this->googleApikey;
    if ($response = file_get_contents($request_google)) {
      $location_data = json_decode($response, TRUE);
      if ($location_data['status'] == 'OK') {
        $result->latitude = $location_data['results'][0]['geometry']['location']['lat'];
        $result->longitude = $location_data['results'][0]['geometry']['location']['lng'];
        $result->request = $request_google;
        $result->response = $response;
        $result->postal_code = $postal_code;
        foreach ($location_data['results'][0]['address_components'] as $address_component) {
          if ($address_component['types'][0] == 'locality') {
            $result->city = $address_component['short_name'];
          }
          elseif ($address_component['types'][0] == 'administrative_area_level_1') {
            $result->state = $address_component['long_name'];
          }
          elseif ($address_component['types'][0] == 'country') {
            $result->country = $address_component['long_name'];
          }
        }
      }
      else {
        $result = FALSE;
      }
      if ($batch_processing) {
        usleep($this->googleRequestsDelay);
      }
    }
    else {
      $result = FALSE;
    }
    if (empty( (array) $result)) {
      return FALSE;
    }
    else {
      return $result;
    }
  }

  /**
   * Get the lat/lon from MapQuest Geocoding API based on Google Autosuggestion.
   *
   * @param string $location
   *   Location value provided by Google Autosuggestion.
   * @param boolean $batch_processing
   *   Whether the request is part of a batch processing. If true, it delays before next request.
   *
   * @return object
   *   ::latitude float,
   *   ::longitude float.
   */
  public function geocodingFromAutoSuggestionsMapQuest($location, $batch_processing = FALSE) {
    $result = new \stdClass;
    $request_mapquest = trim($this->mapquestApiUrlLocality);
    $request_mapquest = str_replace('{locality}', rawurlencode($location), $request_mapquest);
    $request_mapquest .= '&key=' . $this->mapquestApikey;
    if ($response = file_get_contents($request_mapquest)) {
      $location_data = json_decode($response, TRUE);
      if ($location_data['info']['statuscode'] == 0) {
        foreach ($location_data['results'][0]['locations'] as $address_components) {
          if (preg_match('/ZIP/', $address_components['geocodeQuality'])) {
            $result->request = $request_mapquest;
            $result->response = $response;
            $result->latitude = $address_components['latLng']['lat'];
            $result->longitude = $address_components['latLng']['lng'];
            foreach (array_reverse($address_components) as $key => $value) {
              $p_key = str_replace('Type', '', $key);
              if ($key == 'postalCode') {
                $result->postal_code = $value;
              }
              elseif (isset($address_components[$p_key])) {
                if ($value == 'Country') {
                  $result->country = $this->countryAbbreviations[$address_components[$p_key]];
                }
                elseif ($value == 'State') {
                  $result->state = $this->statesAbbreviations[strtolower($result->country)][$address_components[$p_key]];
                }
                elseif ($value == 'City') {
                  $result->city = $address_components[$p_key];
                }
              }
            }
            break;
          }
        }
      }
      else {
        $result = FALSE;
      }
      if ($batch_processing) {
        usleep($this->mapquestRequestsDelay);
      }
    }
    else {
      $result = FALSE;
    }
    if (empty( (array) $result)) {
      return FALSE;
    }
    else {
      return $result;
    }
  }

  /**
   * Get the lat/lon from MapQuest API based on postal code and country.
   *
   * @param string $postal_code
   *   Postal Code.
   * @param string $country
   *   Country.
   * @param boolean $batch_processing
   *   Whether the request is part of a batch processing. If true, it delays before next request.
   *
   * @return object
   *   ::latitude float,
   *   ::longitude float.
   */
  public function geocodingFromPostalCodeMapQuest($postal_code, $country = 'United States', $batch_processing = FALSE) {
    $result = $result = new \stdClass;
    $request_mapquest = trim($this->mapquestApiUrlPostalCode);
    $request_mapquest = str_replace('{postal_code}', rawurlencode($postal_code), $request_mapquest);
    $request_mapquest = str_replace('{country}', rawurlencode($country), $request_mapquest);
    $request_mapquest .= '&key=' . $this->mapquestApikey;
    if ($response = file_get_contents($request_mapquest)) {
      $location_data = json_decode($response, TRUE);
      if ($location_data['info']['statuscode'] == 0) {
        foreach ($location_data['results'][0]['locations'] as $address_components) {
          if (preg_match('/ZIP/', $address_components['geocodeQuality'])) {
            $result->request = $request_mapquest;
            $result->response = $response;
            $result->latitude = $address_components['latLng']['lat'];
            $result->longitude = $address_components['latLng']['lng'];
            $result->postal_code = $postal_code;
            $result->country = $country;
            $geocode_quality = NULL;
            $address_components = $location_data['results'][0]['locations'][0];
            foreach ($address_components as $key => $value) {
              $p_key = str_replace('Type', '', $key);
              if (isset($address_components[$p_key])) {
                if ($value == 'City') {
                  $result->city = $address_components[$p_key];
                }
                elseif ($value == 'State') {
                  $result->state = $this->statesAbbreviations[strtolower($country)][$address_components[$p_key]];
                }
              }
            }
          }
        }

      }
      else {
        $result = FALSE;
      }
      if ($batch_processing) {
        usleep($this->mapquestRequestsDelay);
      }
    }
    else {
      $result = FALSE;
    }
    if (empty( (array) $result)) {
      return FALSE;
    }
    else {
      return $result;
    }
  }

  /**
   * Generates a universal unique id.
   *
   * @return string
   *   The uuid.
   */
  protected function generateUuid() {
    $result = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000,
      mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
    return $result;
  }

  /**
   * Provides an associative array with states abbreviations and names.
   *
   * @param string $country
   *   Country abbreviation.
   *
   * @return array
   *   Abbreviations and names.
   */
  public function getStatesNames($country = 'us') {
    $country = strtolower($country);
    $country_set = [
      'ca' => 'canada',
      'mx' => 'mexico',
      'us' => 'united states',
    ];
    return $this->statesAbbreviations[
      ['ca' => 'canada', 'mx' => 'mexico', 'us' => 'united states'][$country]
    ];
  }
}
