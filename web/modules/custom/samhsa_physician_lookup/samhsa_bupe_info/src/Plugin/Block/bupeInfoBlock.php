<?php

namespace Drupal\samhsa_bupe_info\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a block for the Bupe infographic chart.
 *
 * @Block(
 *   id = "samhsa_bupe_info",
 *   admin_label = @Translation("SAMHSA: Bupe Infographic"),
 *   category = @Translation("SAMHSA Custom"),
 * )
 */
class bupeInfoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'bup_info_graphic');
    $query->condition('status', 1);
    $query->sort('created', 'desc');
    $query->range(0, 1);
    $result = $query->execute();

    if (isset($result)) {

     $bupe_info_graphic_items = \Drupal\node\Entity\Node::loadMultiple($result);
     $node = reset( $bupe_info_graphic_items ); // best way to get first element

     $cert30 = intval(current($node->get('field_cert_30_total')->getValue())['value']);
     $cert100 = intval(current($node->get('field_cert_100_total')->getValue())['value']);
     $cert275 = intval(current($node->get('field_cert_275_total')->getValue())['value']);

    } // end if isset node


    return   [
      '#attached' => ['library' => 'samhsa_bupe_info/gcharts' ],
      '#markup' => '<script type="text/javascript">bwns_donutchart( "Current", "bupe_info_graphic", ' . $cert30 . ', ' . $cert100 . ', ' . $cert275 . ');</script><p id="bupe_info_graphic">&nbsp;</p><p>&nbsp;</p>', '#allowed_tags' => ['script','p'] ];

    
  }

}
