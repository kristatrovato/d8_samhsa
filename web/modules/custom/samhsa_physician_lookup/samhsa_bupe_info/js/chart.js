
function bwns_donutchart(TITLE_STRING, ID_NAME, inpNum1, inpNum2, inpNum3) {
                
                              var colors = ["#334b56", "#8a180e", "#C47B12"];
                              var num_30 = inpNum1; 
                              var num_100 = inpNum2; 
                              var num_275 = inpNum3; 
                              var num_tl = num_30 + num_100 + num_275;
                              var TITLE_STRING = " Total: " + num_tl;
                                                    
                              // Actually load the API
    //                              google.load("visualization", "1", { packages : ["corechart"] });
    google.charts.load('current', {'packages':['corechart', 'table', 'geomap']});
google.charts.setOnLoadCallback(drawChart);

//                              google.setOnLoadCallback(drawChart);
                                                
                              function drawChart() {
                                  var data = google.visualization.arrayToDataTable([["Status", "Numbers"], ["100 Patient Certified", num_100], ["30 Patient Certified", num_30], ["275 Patient Certified", num_275]]);
                                                
                                  var options = { chartArea :  { left : 10, top : 20, width : "90%", height : "90%" },
                                                      width : "90%", height : "160", colors : colors, legend :  {
                                                  alignment : "end", position : "labeled", textStyle :  { italic : "true", fontSize : 15 } },
                                               pieSliceText : "value", pieSliceTextStyle :  { color : "#ebddba", fontSize : 13, bold : "true" },
                                                      title : TITLE_STRING, pieHole : 0.1
                                                }; // end options
                                                
                                                // Create a formatter: This example uses object literal notation to define the options.
                                                var formatter = new google.visualization.NumberFormat( { pattern : "#,###" });
                                                formatter.format(data, 1); // Reformat our data.
                                                
                                                var chart = new google.visualization.PieChart(document.getElementById(ID_NAME));
                                                chart.draw(data, options);
                                                
                             } // end drawChart
                      } // end bwns        

