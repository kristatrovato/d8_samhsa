<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 11/10/18
 * Time: 5:27 PM
 */

/**
 * Implements hook_views_query_alter().
 */
function samhsa_physician_search_views_query_alter(Drupal\views\ViewExecutable $view, Drupal\views\Plugin\views\query\Sql $query) {
  $id = $view->id();
  $display = $view->current_display;
  if ($id == 'samhsa_physician_search' && $display == 'default') {
    list($zip, $distance , $city, $state) = $view->args;
    if ($zip && $zip != '_none' && $distance == '_none') {
      $conditions = new \Drupal\Core\Database\Query\Condition('AND');
      $query->addWhere(
        NULL,
        $conditions
          ->condition('samhsa_physician_address.zip', $zip, '=')
      );
    }
    if ($city && $city != '_none') {
      $conditions = new \Drupal\Core\Database\Query\Condition('AND');
      $query->addWhere(
        NULL,
        $conditions
          ->condition('samhsa_physician_address.city', "%$city%", 'LIKE')
      );
    }
    if ($state && $state != '_none') {
      $conditions = new \Drupal\Core\Database\Query\Condition('AND');
      $query->addWhere(
        NULL,
        $conditions
          ->condition('samhsa_physician_address.state', $state, '=')
      );
    }
    if ($zip && $zip != '_none' && $distance != '_none') {
      $geolocation = \Drupal::service('samhsa_latlon_caching.data')->geocodingFromPostalCode($zip);
      $st_field = "(ST_Distance_Sphere(point($geolocation->longitude, $geolocation->latitude), point(longitude, latitude)) * 0.000621371)";
      $query->addField(NULL, $st_field, 'distance');
      $conditions = new \Drupal\Core\Database\Query\Condition('AND');
      $query->addHavingExpression(
        NULL,
        $conditions
          ->condition('distance', $distance, '<=')
      );
    }
  }
}
