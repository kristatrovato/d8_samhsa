<?php

namespace Drupal\samhsa_physician_search\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SamhsaPhysicianSearchForm.
 */
class SamhsaPhysicianSearchForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'samhsa_physician_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $zip = NULL, $distance = NULL, $city = NULL, $state = NULL) {

    $states = [NULL => '- Any -'] + \Drupal::service('samhsa_latlon_caching.data')->getStatesNames();

    $form['zip'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ZIP Code'),
      '#maxlength' => 10,
      '#size' => 64,
      '#default_value' => ($zip && $zip != '_none') ? $zip : NULL,
      '#weight' => '10',
    ];
    $form['distance'] = [
      '#type' => 'select',
      '#title' => $this->t('Distance from ZIP'),
      '#options' => [
        NULL => $this->t('# Miles'),
        10 => $this->t('10 Miles'),
        25 => $this->t('25 Miles'),
        50 => $this->t('50 Miles'),
        100 => $this->t('100 Miles'),
        250 => $this->t('250 Miles'),
      ],
      '#default_value' => ($distance &&  $distance != '_none') ? $distance : 10,
      '#weight' => '20',
    ];
    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => ($city && $city != '_none') ? $city : NULL,
      '#weight' => '30',
    ];
    $form['state'] = [
      '#type' => 'select',
      '#title' => $this->t('State'),
      '#options' => $states,
      '#default_value' => $state ? $state : NULL,
      '#weight' => '40',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#weight' => '98',
    ];
    $form['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset'),
      '#weight' => '99',
    ];

    return $form;
  }

//  /**
//   * {@inheritdoc}
//   */
//  public function validateForm(array &$form, FormStateInterface $form_state) {
//    parent::validateForm($form, $form_state);
//  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#id'] == 'edit-submit') {
      $arguments = [
        'zip' => $form_state->getValue('zip'),
        'distance' => $form_state->getValue('distance'),
        'city' => $form_state->getValue('city'),
        'state' => $form_state->getValue('state'),
      ];
    }
    else {
      $arguments = [
        'zip' => NULL,
        'distance' => NULL,
        'city' => NULL,
        'state' => NULL,
      ];
    }
    foreach ($arguments as &$argument) {
      if (!$argument) {
        $argument = '_none';
      }
    }
    $form_state->setRedirect('samhsa_physician_search.results', $arguments);
  }

}
