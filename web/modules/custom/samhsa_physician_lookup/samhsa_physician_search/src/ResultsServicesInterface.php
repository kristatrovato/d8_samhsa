<?php

namespace Drupal\samhsa_physician_search;

/**
 * Interface ResultsServicesInterface.
 */
interface ResultsServicesInterface {

  /**
   * Perform the search in the database and returns the results rendered.
   *
   * @param string $zip
   *   Postal code filter.
   * @param string $distance
   *   Distance (in miles) filter.
   * @param string $city
   *   City filter.
   * @param string $state
   *   State filter.
   *
   * @return array
   *   Render array containing the results.
   */
  public function getRenderedResults($zip = NULL, $distance = NULL, $city = NULL, $state = NULL);

}
