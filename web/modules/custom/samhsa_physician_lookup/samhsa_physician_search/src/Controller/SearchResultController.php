<?php

namespace Drupal\samhsa_physician_search\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Class SearchResultController.
 */
class SearchResultController extends ControllerBase {

  /**
   * Showresults.
   *
   * @return array
   *   Form array
   */
  public function showResults($zip = NULL, $distance = NULL, $city = NULL, $state = NULL) {

    $results = \Drupal::service('samhsa_physician_search.results')->getRenderedResults($zip, $distance, $city, $state);

    $image_path = file_create_url('public://samhsa_physician_search/usmap-hotspots-2016.png');

    $state_based_search_path = Url::fromRoute('samhsa_physician_search.results', ['zip' => '_none', 'distance' => '_none', 'city' => '_none', 'state' => '_state'])
      ->toString();
    $state_based_search_path = str_replace('_state', NULL, $state_based_search_path);
    return [
      '#theme' => 'samhsa_physician_search',
      '#map_path' => $image_path,
      '#state_based_search_path' => $state_based_search_path,
      '#search_form' => \Drupal::formBuilder()->getForm('Drupal\samhsa_physician_search\Form\SamhsaPhysicianSearchForm', $zip, $distance, $city, $state),
      '#results' => $results,
    ];

  }

}
