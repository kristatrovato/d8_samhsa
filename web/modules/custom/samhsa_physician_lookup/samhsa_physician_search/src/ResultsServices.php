<?php

namespace Drupal\samhsa_physician_search;

use Drupal\views\Views;

/**
 * Class ResultsServices.
 */
class ResultsServices implements ResultsServicesInterface {

  /**
   * Constructs a new ResultsServices object.
   */
  public function __construct() {

  }

  /**
   * @inheritdoc.
   */
  public function getRenderedResults($zip = NULL, $distance = NULL, $city = NULL, $state = NULL) {
    if ($view = Views::getView('samhsa_physician_search')) {
      $args = [$zip, $distance, $city, $state];
      $view->setArguments($args);
      $view->setDisplay('default');
      $view->preExecute();
      $view->execute();
      $results = $view->buildRenderable('default', $args);
    }
    else {
      $results = $this->t('View not found.');
    }
    return $results;
  }
}
