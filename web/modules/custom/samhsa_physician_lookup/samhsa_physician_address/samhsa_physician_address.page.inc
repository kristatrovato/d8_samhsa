<?php

/**
 * @file
 * Contains samhsa_physician_address.page.inc.
 *
 * Page callback for Samhsa Physician Address entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Samhsa Physician Address templates.
 *
 * Default template: samhsa_physician_address.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_samhsa_physician_address(array &$variables) {
  // Fetch SamhsaPhysicianAddress Entity Object.
  $samhsa_physician_address = $variables['elements']['#samhsa_physician_address'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
