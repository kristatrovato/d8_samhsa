<?php

namespace Drupal\samhsa_physician_address;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Samhsa Physician Address entity.
 *
 * @see \Drupal\samhsa_physician_address\Entity\SamhsaPhysicianAddress.
 */
class SamhsaPhysicianAddressAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\samhsa_physician_address\Entity\SamhsaPhysicianAddressInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished samhsa physician address entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published samhsa physician address entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit samhsa physician address entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete samhsa physician address entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add samhsa physician address entities');
  }

}
