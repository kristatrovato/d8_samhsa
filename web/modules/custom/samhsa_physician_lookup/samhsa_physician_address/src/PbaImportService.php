<?php

namespace Drupal\samhsa_physician_address;


/**
 * @TODO: Should this realy be a service?
 */


/**
 * Class PbaImportService.
 */
class PbaImportService {

  private $fieldIndex = [];

  /**
   * Constructs a new PbaImportServices object.
   */
  public function __construct() {
    $this->fieldIndex = array_flip($_SESSION['_pba_batch']['columns_names']);
  }

  /**
   * Import the data.
   *
   * @param array $rows
   *   Data pulled from the CSV.
   */
  public function importRows(array $rows) {
    foreach ($rows as $row) {
      // Avoid empty lines.
      if ($row == NULL || $row[0] == NULL) {
        continue;
      }
      // Get the index of the ID in row.
      $id_index = $this->fieldIndex['addressId'];
      // Get the ID of the physician.
      $id = $row[$id_index];
      // Check if a record for the physician already exists in the database.
      if ($register_exists = \Drupal::service('samhsa_physician_address.db_operations')->physicianExists($id)) {
        \Drupal::service('samhsa_physician_address.db_operations')->updateAddress($row);
      }
      else {
        \Drupal::service('samhsa_physician_address.db_operations')->createAddress($row);
      }
      // Stores the processed ID in the auxiliary table.
      \Drupal::service('samhsa_physician_address.db_operations')->registerBatchId($id);
    }
  }

}
