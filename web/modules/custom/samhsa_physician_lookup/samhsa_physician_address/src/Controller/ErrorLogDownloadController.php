<?php

namespace Drupal\samhsa_physician_address\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ErrorLogDownloadController.
 */
class ErrorLogDownloadController extends ControllerBase {

  /**
   * ErrorLogDownloadController constructor.
   */
  public function __construct() {
    \Drupal::service('page_cache_kill_switch')->trigger();
  }

  /**
   * Downloade rror log.
   *
   * @return array
   *   Return Hello string.
   */
  public function downloadErrorLog() {
    $csv_path = _samhsa_physician_address_get_file_path();
    $file_path = dirname($csv_path) . '/error_log.csv';
    if ($content = file_get_contents($file_path)) {
      header('Content-Description: File Transfer');
      header('Content-Type:text/csv');
      header('Content-Disposition: attachment; filename="' . basename($file_path) . '"');
      header('Content-Length: ' . strlen($content));
      print $content;
      flush();
    }
    return [];
  }

}
