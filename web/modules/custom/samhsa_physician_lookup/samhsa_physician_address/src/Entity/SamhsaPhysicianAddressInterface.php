<?php

namespace Drupal\samhsa_physician_address\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Samhsa Physician Address entities.
 *
 * @ingroup samhsa_physician_address
 */
interface SamhsaPhysicianAddressInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Samhsa Physician Address name.
   *
   * @return string
   *   Name of the Samhsa Physician Address.
   */
  public function getName();

  /**
   * Sets the Samhsa Physician Address name.
   *
   * @param string $name
   *   The Samhsa Physician Address name.
   *
   * @return \Drupal\samhsa_physician_address\Entity\SamhsaPhysicianAddressInterface
   *   The called Samhsa Physician Address entity.
   */
  public function setName($name);

  /**
   * Gets the Samhsa Physician Address creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Samhsa Physician Address.
   */
  public function getCreatedTime();

  /**
   * Sets the Samhsa Physician Address creation timestamp.
   *
   * @param int $timestamp
   *   The Samhsa Physician Address creation timestamp.
   *
   * @return \Drupal\samhsa_physician_address\Entity\SamhsaPhysicianAddressInterface
   *   The called Samhsa Physician Address entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Samhsa Physician Address published status indicator.
   *
   * Unpublished Samhsa Physician Address are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Samhsa Physician Address is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Samhsa Physician Address.
   *
   * @param bool $published
   *   TRUE to set this Samhsa Physician Address to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\samhsa_physician_address\Entity\SamhsaPhysicianAddressInterface
   *   The called Samhsa Physician Address entity.
   */
  public function setPublished($published);

}
