<?php

namespace Drupal\samhsa_physician_address\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Samhsa Physician Address entities.
 */
class SamhsaPhysicianAddressViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
