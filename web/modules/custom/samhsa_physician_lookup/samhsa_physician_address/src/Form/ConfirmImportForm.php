<?php
/**
 * Created by PhpStorm.
 * User: rmachado
 * Date: 11/6/18
 * Time: 5:04 PM
 */

namespace Drupal\samhsa_physician_address\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form to confirm importing manually.
 */
class ConfirmImportForm extends ConfirmFormBase {
  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $this->id = $id;
    $form['additional_message'] = [
      '#type' => 'item',
      '#markup' => $this->t('This operation can take several minutes.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    _samhsa_physician_address_batch_import();
    $url = new Url('samhsa_physician_address.samhsa_physician_address_config_form');
    return $form_state->setRedirectUrl($url);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "confirm_importinge_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('samhsa_physician_address.samhsa_physician_address_config_form');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Confirm the importing of the CSV file.');
  }

}
