<?php

namespace Drupal\samhsa_physician_address\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Class SamhsaPhysicianAddressConfigForm.
 */
class SamhsaPhysicianAddressConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'samhsa_physician_address.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'samhsa_physician_address_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('samhsa_physician_address.config');

    $form['api_tasks'] = [
      '#type' => 'details',
      '#title' => $this->t('API'),
      '#open' => TRUE,
    ];

    $form['api_tasks']['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API endpoint'),
      '#description' => $this->t('The URL for the feeding API.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('api_endpoint'),
    ];
    $form['api_tasks']['api_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('API call timeout'),
      '#description' => $this->t('Time, in seconds, before triggering a timeout error on the API calling.'),
      '#min' => 0,
      '#default_value' => $config->get('api_timeout'),
    ];


    $form['api_tasks']['file_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to CSV file'),
      '#description' => $this->t('Should be relative to the public files folder.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('file_path'),
    ];

    $form['importing_tasks'] = [
      '#type' => 'details',
      '#title' => $this->t('Importing'),
      '#open' => TRUE,
    ];

    $form['importing_tasks']['cron_interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Cron interval'),
      '#description' => $this->t('Interval, in minutes, between two importing executions.'),
      '#min' => 0,
      '#default_value' => $config->get('cron_interval'),
    ];

    $form['importing_tasks']['error_reports_download'] = [
      '#type' => 'link',
      '#title' => $this->t('Download report of errors found during the last importing process'),
      '#url' => Url::fromRoute('samhsa_physician_address.error_log_download'),
    ];

    $form['importing_tasks']['import_manually'] = [
      '#type' => 'details',
      '#title' => $this->t('Import Manually'),
      '#open' => FALSE,
    ];

    $form['importing_tasks']['import_manually']['button_markup'] = [
      '#type' => 'item',
      '#markup' => $this->t('Press this button to import manually'),
    ];

    $form['importing_tasks']['import_manually']['import'] = [
      '#type' => 'submit',
      '#name' => 'import_button',
      '#value' => $this->t('Import manually'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('samhsa_physician_address.config')
      ->set('api_endpoint', $form_state->getValue('api_endpoint'))
      ->set('api_timeout', $form_state->getValue('api_timeout'))
      ->set('file_path', $form_state->getValue('file_path'))
      ->set('cron_interval', $form_state->getValue('cron_interval'))
      ->save();

    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#name'] == 'import_button') {
      $url = Url::fromRoute('samhsa_physician_address.confirm_import');
      return $form_state->setRedirectUrl($url);
    }
    else {
      parent::submitForm($form, $form_state);
    }
  }

}
