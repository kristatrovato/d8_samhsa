<?php

namespace Drupal\samhsa_physician_address\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Samhsa Physician Address entities.
 *
 * @ingroup samhsa_physician_address
 */
class SamhsaPhysicianAddressDeleteForm extends ContentEntityDeleteForm {


}
