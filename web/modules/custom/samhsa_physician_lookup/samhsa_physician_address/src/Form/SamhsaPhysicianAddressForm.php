<?php

namespace Drupal\samhsa_physician_address\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Samhsa Physician Address edit forms.
 *
 * @ingroup samhsa_physician_address
 */
class SamhsaPhysicianAddressForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\samhsa_physician_address\Entity\SamhsaPhysicianAddress */
    $form = parent::buildForm($form, $form_state);

    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Samhsa Physician Address.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Samhsa Physician Address.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.samhsa_physician_address.canonical', ['samhsa_physician_address' => $entity->id()]);
  }

}
