<?php

namespace Drupal\samhsa_physician_address;

/**
 * Class PbaDbOperationsService.
 */
class PbaDbOperationsService {

  private $database;
  private $fieldIndex = [];
  private $fieldMap = [
    'addressId' => 'id',
    'namePrefix' => 'name_prefix',
    'nameFirst' => 'first_name',
    'nameMiddle' => 'middle_name',
    'nameLast' => 'last_name',
    'nameSuffix' => 'name_suffix',
    'addressLine1' => 'address_line_1',
    'addressLine2' => 'address_line_2',
    'city' => 'city',
    'state' => 'state',
    'zipCode' => 'zip',
    'county' => 'county',
    'geocodeSource' => 'geocode_source',
    'latitude' => 'latitude',
    'longitude' => 'longitude',
    'telephone' => 'telephone',
    'fax' => 'fax',
    'reachedPatientLimit' => 'reached_patient_limit',
    'certifiedFor100' => 'certified_for_100',
  ];
  private $fieldMapFlipped = [];

  /**
   * Constructs a new PbaDbOperationsServices object.
   */
  public function __construct($injected_database) {
    $this->database = $injected_database;
    $this->fieldIndex = array_flip($_SESSION['_pba_batch']['columns_names']);
    $this->fieldMapFlipped = array_flip($this->fieldMap);
  }

  /**
   * Check if a record for a physician already exists in the database.
   *
   * @param int $id
   *   Id of the physician address.
   *
   * @return bool
   *   Whether the record exists.
   */
  public function physicianExists($id) {
    try {
      $query = $this->database->select('samhsa_physician_address', 'b');
      $query->addField('b', 'id');
      $query->condition('id', $id);
      $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    }
    catch (\Exception $e) {
      \Drupal::logger('samhsa_physician_address')->error($e->getMessage());
      $_SESSION['_pba_batch']['errors'] = TRUE;
      $result = FALSE;
    }
    return (boolean) $result;
  }

  /**
   * Updates an existent address.
   *
   * @param array $csv_data
   *   Data pulled from CSV row.
   */
  public function updateAddress(array $csv_data) {
    // Get the index of the ID in row.
    $id_index = $this->fieldIndex['addressId'];
    $fields = $this->populateFields($csv_data);
    $fields['changed'] = time();
    unset($fields['id']);
    unset($fields['uuid']);
    unset($fields['langcode']);
    unset($fields['created']);
    try {
      $query = $this->database->update('samhsa_physician_address');
      $query->fields($fields);
      $query->condition('id', $csv_data[$id_index]);
      $query->execute();
    }
    catch (\Exception $e) {
      \Drupal::logger('samhsa_physician_address')->error($e->getMessage());
      $_SESSION['_pba_batch']['errors'] = TRUE;
      $this->loggingError($e, $csv_data);
    }
  }

  /**
   * Creates a new address.
   *
   * @param array $csv_data
   *   Data pulled from CSV row.
   */
  public function createAddress(array $csv_data) {
    try {
      $fields = $this->populateFields($csv_data);
      $query = $this->database->insert('samhsa_physician_address');
      $query->fields($fields);
      $query->execute();
    }
    catch (\Exception $e) {
      \Drupal::logger('samhsa_physician_address')->error($e->getMessage());
      $_SESSION['_pba_batch']['errors'] = TRUE;
      $this->loggingError($e, $csv_data);
    }
  }

  /**
   * Writes the error message in a file.
   *
   * @param \Exception $e
   *   Exception object.
   * @param array $csv_data
   *   Array with the CSV row data.
   */
  private function loggingError(\Exception $e, array $csv_data) {
    $problem_col = 'N/A';
    // Is the exception coming from PDO?
    if ($message = $e->getPrevious()->getMessage()) {
      // Get the problematic column.
      $am = explode("'", $message);
      if (isset($am[1])) {
        $problem_col = $am[1];
      }
    }
    else {
      $e->getMessage();
    }
    $id_index = $this->fieldIndex['addressId'];
    $problem_col_index = $this->fieldIndex[$this->fieldMapFlipped[$problem_col]];
    $log = [
      'address_id' => $csv_data[$id_index],
      'column' => $problem_col,
      'value' => $csv_data[$problem_col_index],
      'original_message' => $message,
    ];
    $_SESSION['_pba_batch']['error_log'][] = $log;
  }

  /**
   * Creates a new address.
   *
   * @param array $csv_data
   *   Data pulled from CSV row.
   *
   * @return array
   *   Associative array: field_name => value.
   */
  private function populateFields(array $csv_data) {
    $result = [];
    foreach ($this->fieldMap as $csv_column_name => $db_field_name) {
      // Get the index of the column.
      $column_index = $this->fieldIndex[$csv_column_name];
      // Check if there's specific method to convert the column's value.
      $method_name = 'formatValueOf' . ucfirst($csv_column_name);
      if (method_exists($this, $method_name)) {
        $val = $this->$method_name($csv_data[$column_index]);
      }
      else {
        $val = $csv_data[$column_index];
      }
      $result[$db_field_name] = $val;
    }
    if ($result['middle_name']) {
      $result['name'] = "{$result['first_name']} {$result['middle_name']} {$result['last_name']}";
    }
    else {
      $result['name'] = "{$result['first_name']} {$result['last_name']}";
    }
    $result['uuid'] = $this->generateUuid();
    $result['langcode'] = 'en';
    $result['status'] = 1;
    $result['created'] = time();
    $result['user_id'] = \Drupal::currentUser()->id();
    return $result;
  }

  /**
   * Generate a universal unique identifier.
   *
   * @return string
   *   The UUID.
   */
  private function generateUuid() {
    $uuid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff), mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000,
      mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );
    return $uuid;
  }

  /**
   * Convert value of latitude to float.
   *
   * @param string $val
   *   Value pulled from the CSV.
   *
   * @return float
   *   Converted value.
   */
  private function formatValueOfLatitude($val) {
    return (float) $val;
  }

  /**
   * Convert value of longitude to float.
   *
   * @param string $val
   *   Value pulled from the CSV.
   *
   * @return float
   *   Converted value.
   */
  private function formatValueOfLongitude($val) {
    return (float) $val;
  }

  /**
   * Convert value of reachedPatientLimit to tinyint.
   *
   * @param string $val
   *   Value pulled from the CSV.
   *
   * @return int
   *   Converted value.
   */
  private function formatValueOfReachedPatientLimit($val) {
    return strtolower($val) == 'y' ? 1 : 0;
  }

  /**
   * Convert value of certifiedFor100 to tinyint.
   *
   * @param string $val
   *   Value pulled from the CSV.
   *
   * @return int
   *   Converted value.
   */
  private function formatValueOfCertifiedFor100($val) {
    return strtolower($val) == 'y' ? 1 : 0;
  }

  /**
   * Truncates the table that stores the processed IDs.
   */
  public function truncateBatchIds() {
    $query = $this->database->truncate('samhsa_physician_address_batch_ids');
    $query->execute();
  }

  /**
   * Register the ID of the physician's address being processed.
   *
   * @param int $id
   *   ID if the record.
   */
  public function registerBatchId($id) {
    try {
      $query = $this->database->insert('samhsa_physician_address_batch_ids');
      $query->fields(['id' => $id]);
      $query->execute();
    }
    catch (\Exception $e) {
      \Drupal::logger('samhsa_physician_address')->error($e->getMessage());
      $_SESSION['_pba_batch']['errors'] = TRUE;
    }
  }

  /**
   * Remove IDs in the main table that are not present in the auxiliary table.
   */
  public function removeInexistentIds() {
    // Check if there are processed IDs before effectively process the removal.
    if ($this->countBatchIds()) {
      try {
        $query = $this->database->query('DELETE b from samhsa_physician_address AS b 
        LEFT JOIN samhsa_physician_address_batch_ids AS bid ON bid.id = b.id
        WHERE bid.id IS NULL;');
        $query->execute();
      }
      catch (\Exception $e) {
        \Drupal::logger('samhsa_physician_address')->error($e->getMessage());
        $_SESSION['_pba_batch']['errors'] = TRUE;
      }
    }
  }

  /**
   * Remove IDs in the main table that are not present in the auxiliary table.
   */
  public function countBatchIds() {
    try {
      $query = $this->database->select('samhsa_physician_address_batch_ids');
      $result = $query->countQuery()->execute()->fetchField();
    }
    catch (\Exception $e) {
      \Drupal::logger('samhsa_physician_address')->error($e->getMessage());
      $_SESSION['_pba_batch']['errors'] = TRUE;
      $result = 0;
    }
    return $result;
  }

}

