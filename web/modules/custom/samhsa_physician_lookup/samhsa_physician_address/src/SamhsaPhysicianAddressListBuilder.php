<?php

namespace Drupal\samhsa_physician_address;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Samhsa Physician Address entities.
 *
 * @ingroup samhsa_physician_address
 */
class SamhsaPhysicianAddressListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Samhsa Physician Address ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\samhsa_physician_address\Entity\SamhsaPhysicianAddress */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.samhsa_physician_address.edit_form',
      ['samhsa_physician_address' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
