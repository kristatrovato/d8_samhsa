#!/bin/bash

drush migrate-import upgrade_d7_node_in_the_news --update

drush migrate-import upgrade_d7_node_revision_in_the_news --update

drush migrate-import upgrade_d7_node_in_the_news_update --update
