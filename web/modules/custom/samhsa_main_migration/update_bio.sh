#!/bin/bash

drush migrate-import upgrade_d7_node_biography --update

drush migrate-import upgrade_d7_node_revision_biography --update

drush migrate-import upgrade_d7_node_biography_update --update
