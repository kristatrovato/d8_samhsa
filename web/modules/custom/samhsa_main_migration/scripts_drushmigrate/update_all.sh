#!/bin/bash

drush migrate-import upgrade_d7_node_biography --update
drush migrate-import upgrade_d7_node_revision_biography --update
#drush migrate-import upgrade_d7_node_biography_update --update

drush migrate-import upgrade_d7_node_budget
drush migrate-import upgrade_d7_node_revision_budget --update
#drush migrate-import upgrade_d7_node_budget_update --update

drush migrate-import upgrade_d7_node_ebp_resource --update
#drush migrate-import upgrade_d7_node_ebp_resource_update --update

drush migrate-import upgrade_d7_node_fep_program --update
#drush migrate-import upgrade_d7_node_fep_program_update --update

drush migrate-import upgrade_d7_node_rfa --update
drush migrate-import upgrade_d7_node_revision_rfa --update
#drush migrate-import  upgrade_d7_node_rfa_update --update

drush migrate-import upgrade_d7_node_gains_mhc --update
#drush migrate-import upgrade_d7_node_gains_mhc_update --update

drush migrate-import upgrade_d7_node_gains_trauma_trainers --update
drush migrate-import upgrade_d7_node_gains_trauma_trainers_csv --update
drush migrate-import upgrade_d7_node_gains_trauma_trainers_update --update

drush migrate-import upgrade_d7_node_generic --update
drush migrate-import upgrade_d7_node_revision_generic --update
#drush migrate-import upgrade_d7_node_generic_update --update

drush migrate-import upgrade_d7_node_generic_org_structure --update
drush migrate-import upgrade_d7_node_revision_generic_org_structure --update
#drush migrate-import upgrade_d7_node_generic_org_structure_update --update

drush migrate-import upgrade_d7_node_geographic_contacts --update
#drush migrate-import upgrade_d7_node_geographic_contacts_update --update

drush migrate-import upgrade_d7_node_grant_awards_by_state --update
#drush migrate-import upgrade_d7_node_grant_awards_by_state_update --update

drush migrate-import upgrade_d7_node_grants_awards --update
#drush migrate-import upgrade_d7_node_grants_awards_update --update

drush migrate-import upgrade_d7_node_grant_state_details --update
#drush migrate-import upgrade_d7_node_grant_state_details_update --update

drush migrate-import upgrade_d7_node_hpr_resource --update
#missing revisions??
#drush migrate-import upgrade_d7_node_hpr_resource_update --update

drush migrate-import upgrade_d7_node_hpr_video --update
drush migrate-import upgrade_d7_node_hpr_video_csv --update
drush migrate-import upgrade_d7_node_revision_hpr_video --update
drush migrate-import upgrade_d7_node_hpr_video_update --update

drush migrate-import upgrade_d7_node_infographic --update
#drush migrate-import upgrade_d7_node_infographic_update --update

drush migrate-import upgrade_d7_node_in_the_news --update
drush migrate-import upgrade_d7_node_revision_in_the_news --update
#drush migrate-import upgrade_d7_node_in_the_news_update --update

drush migrate-import upgrade_d7_node_landing_page --update
drush migrate-import upgrade_d7_node_revision_landing_page --update
#drush migrate-import upgrade_d7_node_landing_update --update

drush migrate-import upgrade_d7_node_landing_programs_campaigns --update
drush migrate-import upgrade_d7_node_revision_landing_programs_campaigns --update
#drush migrate-import upgrade_d7_node_landing_programs_campaigns_update --update

drush migrate-import upgrade_d7_node_meeting --update
drush migrate-import upgrade_d7_node_revision_meeting --update
#drush migrate-import upgrade_d7_node_meeting_update --update

drush migrate-import upgrade_d7_node_news --update
drush migrate-import upgrade_d7_node_revision_news --update
#drush migrate-import upgrade_d7_node_news_update --update

drush migrate-import upgrade_d7_node_news_link --update
drush migrate-import upgrade_d7_node_revision_news_link --update
#drush migrate-import upgrade_d7_node_news_link_update --update

drush migrate-import upgrade_d7_node_slide
drush migrate-import upgrade_d7_node_revision_slide --update
#drush migrate-import upgrade_d7_node_slide_update --update

drush migrate-import upgrade_d7_node_wellness_event --update
#drush migrate-import upgrade_d7_node_wellness_event_update --update

#drush migrate-import upgrade_d7_node_homepage_item --update
#drush migrate-import upgrade_d7_node_homepage_item_update --update

drush migrate-import upgrade_d7_menu_links --update
drush migrate-import upgrade_d7_url_alias --update