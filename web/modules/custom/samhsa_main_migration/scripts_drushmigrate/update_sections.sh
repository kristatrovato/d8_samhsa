#!/bin/bash

drush migrate-import upgrade_d7_node_biography_section_csv -y
drush migrate-import upgrade_d7_node_biography_update --update -y

drush migrate-import upgrade_d7_node_budget_section_csv -y
drush migrate-import upgrade_d7_node_budget_update --update -y

drush migrate-import upgrade_d7_node_ebp_resource_section_csv -y
drush migrate-import upgrade_d7_node_ebp_resource_update --update -y

drush migrate-import upgrade_d7_node_fep_program_section_csv -y
drush migrate-import upgrade_d7_node_fep_program_update --update -y

drush migrate-import upgrade_d7_node_rfa_section_csv -y
drush migrate-import  upgrade_d7_node_rfa_update --update -y

#no gains mhc

drush migrate-import upgrade_d7_node_gains_trauma_trainers_csv -y
drush migrate-import upgrade_d7_node_gains_trauma_trainers_csv --update -y
drush migrate-import upgrade_d7_node_gains_trauma_trainers_update --update -y

drush migrate-import upgrade_d7_node_generic_section_csv -y
drush migrate-import upgrade_d7_node_generic_section_acws_csv -y
drush migrate-import upgrade_d7_node_generic_update --update -y

drush migrate-import upgrade_d7_node_generic_org_structure_section_csv -y
drush migrate-import upgrade_d7_node_generic_org_structure_update --update -y

drush migrate-import upgrade_d7_node_geographic_contacts_section_csv -y
drush migrate-import upgrade_d7_node_geographic_contacts_update --update -y

drush migrate-import upgrade_d7_node_hpr_resource_section_csv -y
drush migrate-import upgrade_d7_node_hpr_resource_update --update -y

drush migrate-import upgrade_d7_node_hpr_video_section_csv -y
drush migrate-import upgrade_d7_node_hpr_video_update --update -y

drush migrate-import upgrade_d7_node_infographic_section_csv -y
drush migrate-import upgrade_d7_node_infographic_update --update -y

#no in the news

drush migrate-import upgrade_d7_node_landing_page_section_csv -y
drush migrate-import upgrade_d7_node_landing_update --update -y

drush migrate-import upgrade_d7_node_landing_programs_campaigns_section_csv -y
drush migrate-import upgrade_d7_node_landing_programs_campaigns_section_spanish -y
drush migrate-import upgrade_d7_node_landing_programs_campaigns_update --update -y

drush migrate-import upgrade_d7_node_meeting_section_csv -y
drush migrate-import upgrade_d7_node_meeting_section_acws_csv -y
drush migrate-import upgrade_d7_node_meeting_update --update -y

drush migrate-import upgrade_d7_node_news_section_csv -y
drush migrate-import upgrade_d7_node_news_update --update -y

drush migrate-import upgrade_d7_node_news_link_section_csv -y
drush migrate-import upgrade_d7_node_news_link_update --update -y

drush migrate-import upgrade_d7_node_slide_section_csv -y
drush migrate-import upgrade_d7_node_slide_update --update -y

drush migrate-import upgrade_d7_node_wellness_event_section_csv -y
drush migrate-import upgrade_d7_node_wellness_event_update --update -y

#drush migrate-import upgrade_d7_node_homepage_item_section_csv -y
#drush migrate-import upgrade_d7_node_homepage_item_update --update -y

#drush migrate-import upgrade_d7_menu_links --update
#drush migrate-import upgrade_d7_url_alias --update