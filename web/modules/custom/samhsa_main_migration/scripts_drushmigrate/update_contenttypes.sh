#!/bin/bash

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_biography/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_budget/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_ebp_resource/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_fep_program/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_gains_mhc/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_gains_trauma_trainers/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_generic/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_generic_org_structure/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_geographic_contacts/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_grant_awards_by_state/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_grant_state_details/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_grants_awards/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_homepage_item/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_hpr_resource/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_hpr_video/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_infographic/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_in_the_news/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_landing_page/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_landing_programs_campaigns/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_meeting/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_news/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_news_link/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_rfa/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_slide/config/install -y

drush config:import --partial --source=/srv/dev11/d8_samhsa/web/modules/custom/samhsa_main_migration/samhsa_wellness_event/config/install -y
