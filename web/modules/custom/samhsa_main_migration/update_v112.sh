#!/bin/bash

./update_short.sh

drush migrate:stop upgrade_d7_file
drush migrate:reset upgrade_d7_file

drush migrate:stop upgrade_d7_node_wellness_event
drush migrate:reset upgrade_d7_node_wellness_event

drush migrate:rollback upgrade_d7_file
drush migrate:rollback upgrade_d7_file_private
drush migrate:rollback upgrade_d7_node_wellness_event

drush migrate:rollback upgrade_d7_node_grant_awards_by_state_update
drush migrate:rollback upgrade_d7_node_grant_awards_by_state

drush migrate:rollback upgrade_d7_node_grants_awards_update
drush migrate:rollback upgrade_d7_node_grants_awards

drush migrate:rollback upgrade_d7_node_grant_state_details_update
drush migrate:rollback upgrade_d7_node_grant_state_details

drush migrate-manifest --legacy-db-url=mysql://dev:samhsa@localhost/samhsaprod /var/www/html/d8_samhsa/web/modules/custom/samhsa_main_migration/manifest-section.yml
drush migrate-manifest --legacy-db-url=mysql://dev:samhsa@localhost/samhsaprod /var/www/html/d8_samhsa/web/modules/custom/samhsa_main_migration/manifest-5.yml
