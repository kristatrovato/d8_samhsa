#!/bin/bash

drush migrate-import upgrade_d7_node_landing_page --update

drush migrate-import upgrade_d7_node_revision_landing_page --update

drush migrate-import upgrade_d7_node_landing_update --update
