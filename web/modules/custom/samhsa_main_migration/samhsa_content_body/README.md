#samhsa_content_body
This hooks the migration process to allow the *text* and *summary* parts of the body field to be populated using separate sources.
##samhsa_content_body.module

**hook_migrate_prepare_row()** 
- The migration ID is compared to an inline list. If there is a match
- A number of tests are assessed to determine whether this migration row is a candidate for the manipulation. **The primary consideration is whether or not there is body summary content:**
    - The summary source must be an array
    - The array must have an entry [0]
    - Entry [0] must be an array
    - Entry [0]['value'] must not be empty - *Note: the summary content for the node is being kept in a body-like field as its main text, so it will be in the *value* portion of that field, **not** the summary portion*
- If there is summary content, the body is manipulated in whichever way necessary to be viable:
    - *body* must be an array, **otherwise create one and an entry**
    - *body* must have array content
    - the first element in the body array must be an array, **otherwise one is created**
    - the *value* entry of the body element cannot be empty
    
**NOTE: the summary source is hard-coded to be *field_summary* in every case.**    