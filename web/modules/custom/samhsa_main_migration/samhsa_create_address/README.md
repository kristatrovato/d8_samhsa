#samhsa_create_address

- The **source** field should be a **D7** field of type *Location* (a la the Location module)
- The **destination** field should a **D8** field be of type *Address* (a la the Address module)
- The entry for the destination field should look like:
```
  d8_field_name:
    -
      plugin: samhsa_create_address
      source: d7_field_name
```
##src/Plugin/migrate/process/SamhsaCreateAddress.php
* The source (D7 location field) is a reference, and so contains a LID (location ID)
* The location entry is retrieved in ::getLocationRec via a query
* The address records (node and node revision) are created in ::createAddressRec via insert query