$view = new view();
$view->name = 'qa_grant_awards';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'QA Grant Awards';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'QA Grant Awards';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '100';
$handler->display->display_options['style_plugin'] = 'table';
/* Header: Global: Result summary */
$handler->display->display_options['header']['result']['id'] = 'result';
$handler->display->display_options['header']['result']['table'] = 'views';
$handler->display->display_options['header']['result']['field'] = 'result';
/* Relationship: Content: Author */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'long';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
/* Field: Content: Published */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'long';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
/* Field: Content: Activity Code */
$handler->display->display_options['fields']['field_activity_code']['id'] = 'field_activity_code';
$handler->display->display_options['fields']['field_activity_code']['table'] = 'field_data_field_activity_code';
$handler->display->display_options['fields']['field_activity_code']['field'] = 'field_activity_code';
/* Field: Content: Application organization code */
$handler->display->display_options['fields']['field_applctn_org_cd']['id'] = 'field_applctn_org_cd';
$handler->display->display_options['fields']['field_applctn_org_cd']['table'] = 'field_data_field_applctn_org_cd';
$handler->display->display_options['fields']['field_applctn_org_cd']['field'] = 'field_applctn_org_cd';
/* Field: Content: Award amount  */
$handler->display->display_options['fields']['field_rfa_award_amount']['id'] = 'field_rfa_award_amount';
$handler->display->display_options['fields']['field_rfa_award_amount']['table'] = 'field_data_field_rfa_award_amount';
$handler->display->display_options['fields']['field_rfa_award_amount']['field'] = 'field_rfa_award_amount';
$handler->display->display_options['fields']['field_rfa_award_amount']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Field: Content: Award number */
$handler->display->display_options['fields']['field_award_number']['id'] = 'field_award_number';
$handler->display->display_options['fields']['field_award_number']['table'] = 'field_data_field_award_number';
$handler->display->display_options['fields']['field_award_number']['field'] = 'field_award_number';
/* Field: Content: Award Number */
$handler->display->display_options['fields']['field_grant_award_number']['id'] = 'field_grant_award_number';
$handler->display->display_options['fields']['field_grant_award_number']['table'] = 'field_data_field_grant_award_number';
$handler->display->display_options['fields']['field_grant_award_number']['field'] = 'field_grant_award_number';
/* Field: Content: Center */
$handler->display->display_options['fields']['field_grant_center']['id'] = 'field_grant_center';
$handler->display->display_options['fields']['field_grant_center']['table'] = 'field_data_field_grant_center';
$handler->display->display_options['fields']['field_grant_center']['field'] = 'field_grant_center';
/* Field: Content: Congressional District */
$handler->display->display_options['fields']['field_congressional_district']['id'] = 'field_congressional_district';
$handler->display->display_options['fields']['field_congressional_district']['table'] = 'field_data_field_congressional_district';
$handler->display->display_options['fields']['field_congressional_district']['field'] = 'field_congressional_district';
/* Field: Content: FOA Number */
$handler->display->display_options['fields']['field_foa_number']['id'] = 'field_foa_number';
$handler->display->display_options['fields']['field_foa_number']['table'] = 'field_data_field_foa_number';
$handler->display->display_options['fields']['field_foa_number']['field'] = 'field_foa_number';
/* Field: Content: Grant Awarded Year */
$handler->display->display_options['fields']['field_grant_awarded_year']['id'] = 'field_grant_awarded_year';
$handler->display->display_options['fields']['field_grant_awarded_year']['table'] = 'field_data_field_grant_awarded_year';
$handler->display->display_options['fields']['field_grant_awarded_year']['field'] = 'field_grant_awarded_year';
$handler->display->display_options['fields']['field_grant_awarded_year']['delta_offset'] = '0';
/* Field: Content: Grant Funding */
$handler->display->display_options['fields']['field_grant_funding']['id'] = 'field_grant_funding';
$handler->display->display_options['fields']['field_grant_funding']['table'] = 'field_data_field_grant_funding';
$handler->display->display_options['fields']['field_grant_funding']['field'] = 'field_grant_funding';
/* Field: Content: Grant Org */
$handler->display->display_options['fields']['field_grant_org']['id'] = 'field_grant_org';
$handler->display->display_options['fields']['field_grant_org']['table'] = 'field_data_field_grant_org';
$handler->display->display_options['fields']['field_grant_org']['field'] = 'field_grant_org';
/* Field: Content: Grantee name */
$handler->display->display_options['fields']['field_grantee_name']['id'] = 'field_grantee_name';
$handler->display->display_options['fields']['field_grantee_name']['table'] = 'field_data_field_grantee_name';
$handler->display->display_options['fields']['field_grantee_name']['field'] = 'field_grantee_name';
/* Field: Content: Grantee city */
$handler->display->display_options['fields']['field_grantee_city']['id'] = 'field_grantee_city';
$handler->display->display_options['fields']['field_grantee_city']['table'] = 'field_data_field_grantee_city';
$handler->display->display_options['fields']['field_grantee_city']['field'] = 'field_grantee_city';
/* Field: Content: Grantee state */
$handler->display->display_options['fields']['field_grantee_state']['id'] = 'field_grantee_state';
$handler->display->display_options['fields']['field_grantee_state']['table'] = 'field_data_field_grantee_state';
$handler->display->display_options['fields']['field_grantee_state']['field'] = 'field_grantee_state';
/* Field: Content: guid */
$handler->display->display_options['fields']['field_guid']['id'] = 'field_guid';
$handler->display->display_options['fields']['field_guid']['table'] = 'field_data_field_guid';
$handler->display->display_options['fields']['field_guid']['field'] = 'field_guid';
$handler->display->display_options['fields']['field_guid']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Field: Content: Project director first name */
$handler->display->display_options['fields']['field_pd_rev_fname']['id'] = 'field_pd_rev_fname';
$handler->display->display_options['fields']['field_pd_rev_fname']['table'] = 'field_data_field_pd_rev_fname';
$handler->display->display_options['fields']['field_pd_rev_fname']['field'] = 'field_pd_rev_fname';
/* Field: Content: Project director last name */
$handler->display->display_options['fields']['field_pd_rev_lname']['id'] = 'field_pd_rev_lname';
$handler->display->display_options['fields']['field_pd_rev_lname']['table'] = 'field_data_field_pd_rev_lname';
$handler->display->display_options['fields']['field_pd_rev_lname']['field'] = 'field_pd_rev_lname';
/* Field: Content: Project Period */
$handler->display->display_options['fields']['field_project_period']['id'] = 'field_project_period';
$handler->display->display_options['fields']['field_project_period']['table'] = 'field_data_field_project_period';
$handler->display->display_options['fields']['field_project_period']['field'] = 'field_project_period';
/* Field: Content: RFA */
$handler->display->display_options['fields']['field_rfa']['id'] = 'field_rfa';
$handler->display->display_options['fields']['field_rfa']['table'] = 'field_data_field_rfa';
$handler->display->display_options['fields']['field_rfa']['field'] = 'field_rfa';
/* Field: Content: State */
$handler->display->display_options['fields']['field_grant_state']['id'] = 'field_grant_state';
$handler->display->display_options['fields']['field_grant_state']['table'] = 'field_data_field_grant_state';
$handler->display->display_options['fields']['field_grant_state']['field'] = 'field_grant_state';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  8 => 0,
  6 => 0,
  5 => 0,
  10 => 0,
  11 => 0,
  4 => 0,
  7 => 0,
  3 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
);
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'grants_awards' => 'grants_awards',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/qa-grant-awards';
$translatables['qa_grant_awards'] = array(
  t('Master'),
  t('QA Grant Awards'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Displaying @start - @end of @total'),
  t('author'),
  t('Title'),
  t('Nid'),
  t('Post date'),
  t('Published'),
  t('Updated date'),
  t('Name'),
  t('Body'),
  t('Activity Code'),
  t('Application organization code'),
  t('Award amount '),
  t('Award number'),
  t('Award Number'),
  t('Center'),
  t('Congressional District'),
  t('FOA Number'),
  t('Grant Awarded Year'),
  t('Grant Funding'),
  t('Grant Org'),
  t('Grantee name'),
  t('Grantee city'),
  t('Grantee state'),
  t('guid'),
  t('Project director first name'),
  t('Project director last name'),
  t('Project Period'),
  t('RFA'),
  t('State'),
  t('Page'),
);
