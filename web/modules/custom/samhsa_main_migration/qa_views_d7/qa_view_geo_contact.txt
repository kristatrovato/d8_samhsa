$view = new view();
$view->name = 'qa_geographic_contacts';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'QA Geographic Contacts';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'QA Geographic Contacts';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '100';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'nid' => 'nid',
  'title' => 'title',
  'status' => 'status',
  'created' => 'created',
  'changed' => 'changed',
  'alias' => 'alias',
  'name' => 'name',
  'section' => 'section',
  'field_address' => 'field_address',
  'field_address3' => 'field_address3',
  'body' => 'body',
  'field_geo_contact_active' => 'field_geo_contact_active',
  'field_geo_contact_agency' => 'field_geo_contact_agency',
  'field_geo_contact_email' => 'field_geo_contact_email',
  'field_geo_contact_first_name' => 'field_geo_contact_first_name',
  'field_geo_contact_last_name' => 'field_geo_contact_last_name',
  'field_geo_contact_latitude' => 'field_geo_contact_latitude',
  'field_geo_contact_longitude' => 'field_geo_contact_longitude',
  'field_geo_contact_number' => 'field_geo_contact_number',
  'field_geo_contact_office_fax' => 'field_geo_contact_office_fax',
  'field_geo_contact_office_phone' => 'field_geo_contact_office_phone',
  'field_geo_contact_providerid' => 'field_geo_contact_providerid',
  'field_geo_contact_state_name' => 'field_geo_contact_state_name',
  'field_geo_contact_title' => 'field_geo_contact_title',
  'field_geocontact_is_a_state_cont' => 'field_geocontact_is_a_state_cont',
  'field_geo_is_a_state_contact' => 'field_geo_is_a_state_contact',
);
$handler->display->display_options['style_options']['default'] = 'nid';
$handler->display->display_options['style_options']['info'] = array(
  'nid' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'alias' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'section' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_address' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_address3' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'body' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_active' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_agency' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_email' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_first_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_last_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_latitude' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_longitude' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_number' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_office_fax' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_office_phone' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_providerid' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_state_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_contact_title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geocontact_is_a_state_cont' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_geo_is_a_state_contact' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: Result summary */
$handler->display->display_options['header']['result']['id'] = 'result';
$handler->display->display_options['header']['result']['table'] = 'views';
$handler->display->display_options['header']['result']['field'] = 'result';
/* Relationship: Content: Author */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Published */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'long';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'long';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: Node: URL alias */
$handler->display->display_options['fields']['alias']['id'] = 'alias';
$handler->display->display_options['fields']['alias']['table'] = 'views_url_alias_node';
$handler->display->display_options['fields']['alias']['field'] = 'alias';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Author';
/* Field: Workbench Access: Section */
$handler->display->display_options['fields']['section']['id'] = 'section';
$handler->display->display_options['fields']['section']['table'] = 'workbench_access';
$handler->display->display_options['fields']['section']['field'] = 'section';
/* Field: Content: Address */
$handler->display->display_options['fields']['field_address']['id'] = 'field_address';
$handler->display->display_options['fields']['field_address']['table'] = 'field_data_field_address';
$handler->display->display_options['fields']['field_address']['field'] = 'field_address';
/* Field: Content: Address3 */
$handler->display->display_options['fields']['field_address3']['id'] = 'field_address3';
$handler->display->display_options['fields']['field_address3']['table'] = 'field_data_field_address3';
$handler->display->display_options['fields']['field_address3']['field'] = 'field_address3';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
/* Field: Content: Geo Contact Active */
$handler->display->display_options['fields']['field_geo_contact_active']['id'] = 'field_geo_contact_active';
$handler->display->display_options['fields']['field_geo_contact_active']['table'] = 'field_data_field_geo_contact_active';
$handler->display->display_options['fields']['field_geo_contact_active']['field'] = 'field_geo_contact_active';
$handler->display->display_options['fields']['field_geo_contact_active']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Field: Content: Geo Contact Agency */
$handler->display->display_options['fields']['field_geo_contact_agency']['id'] = 'field_geo_contact_agency';
$handler->display->display_options['fields']['field_geo_contact_agency']['table'] = 'field_data_field_geo_contact_agency';
$handler->display->display_options['fields']['field_geo_contact_agency']['field'] = 'field_geo_contact_agency';
/* Field: Content: Geo Contact Email */
$handler->display->display_options['fields']['field_geo_contact_email']['id'] = 'field_geo_contact_email';
$handler->display->display_options['fields']['field_geo_contact_email']['table'] = 'field_data_field_geo_contact_email';
$handler->display->display_options['fields']['field_geo_contact_email']['field'] = 'field_geo_contact_email';
/* Field: Content: Geo Contact First Name */
$handler->display->display_options['fields']['field_geo_contact_first_name']['id'] = 'field_geo_contact_first_name';
$handler->display->display_options['fields']['field_geo_contact_first_name']['table'] = 'field_data_field_geo_contact_first_name';
$handler->display->display_options['fields']['field_geo_contact_first_name']['field'] = 'field_geo_contact_first_name';
/* Field: Content: Geo Contact Last Name */
$handler->display->display_options['fields']['field_geo_contact_last_name']['id'] = 'field_geo_contact_last_name';
$handler->display->display_options['fields']['field_geo_contact_last_name']['table'] = 'field_data_field_geo_contact_last_name';
$handler->display->display_options['fields']['field_geo_contact_last_name']['field'] = 'field_geo_contact_last_name';
/* Field: Content: Geo Contact Latitude */
$handler->display->display_options['fields']['field_geo_contact_latitude']['id'] = 'field_geo_contact_latitude';
$handler->display->display_options['fields']['field_geo_contact_latitude']['table'] = 'field_data_field_geo_contact_latitude';
$handler->display->display_options['fields']['field_geo_contact_latitude']['field'] = 'field_geo_contact_latitude';
$handler->display->display_options['fields']['field_geo_contact_latitude']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Content: Geo Contact Longitude */
$handler->display->display_options['fields']['field_geo_contact_longitude']['id'] = 'field_geo_contact_longitude';
$handler->display->display_options['fields']['field_geo_contact_longitude']['table'] = 'field_data_field_geo_contact_longitude';
$handler->display->display_options['fields']['field_geo_contact_longitude']['field'] = 'field_geo_contact_longitude';
$handler->display->display_options['fields']['field_geo_contact_longitude']['settings'] = array(
  'thousand_separator' => '',
  'decimal_separator' => '.',
  'scale' => '2',
  'prefix_suffix' => 1,
);
/* Field: Content: Geo Contact Number */
$handler->display->display_options['fields']['field_geo_contact_number']['id'] = 'field_geo_contact_number';
$handler->display->display_options['fields']['field_geo_contact_number']['table'] = 'field_data_field_geo_contact_number';
$handler->display->display_options['fields']['field_geo_contact_number']['field'] = 'field_geo_contact_number';
$handler->display->display_options['fields']['field_geo_contact_number']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Field: Content: Geo Contact Office Fax */
$handler->display->display_options['fields']['field_geo_contact_office_fax']['id'] = 'field_geo_contact_office_fax';
$handler->display->display_options['fields']['field_geo_contact_office_fax']['table'] = 'field_data_field_geo_contact_office_fax';
$handler->display->display_options['fields']['field_geo_contact_office_fax']['field'] = 'field_geo_contact_office_fax';
/* Field: Content: Geo Contact Office Phone */
$handler->display->display_options['fields']['field_geo_contact_office_phone']['id'] = 'field_geo_contact_office_phone';
$handler->display->display_options['fields']['field_geo_contact_office_phone']['table'] = 'field_data_field_geo_contact_office_phone';
$handler->display->display_options['fields']['field_geo_contact_office_phone']['field'] = 'field_geo_contact_office_phone';
/* Field: Content: Geo Contact ProviderId */
$handler->display->display_options['fields']['field_geo_contact_providerid']['id'] = 'field_geo_contact_providerid';
$handler->display->display_options['fields']['field_geo_contact_providerid']['table'] = 'field_data_field_geo_contact_providerid';
$handler->display->display_options['fields']['field_geo_contact_providerid']['field'] = 'field_geo_contact_providerid';
$handler->display->display_options['fields']['field_geo_contact_providerid']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Field: Content: Geo Contact State Name */
$handler->display->display_options['fields']['field_geo_contact_state_name']['id'] = 'field_geo_contact_state_name';
$handler->display->display_options['fields']['field_geo_contact_state_name']['table'] = 'field_data_field_geo_contact_state_name';
$handler->display->display_options['fields']['field_geo_contact_state_name']['field'] = 'field_geo_contact_state_name';
/* Field: Content: Geo Contact Title */
$handler->display->display_options['fields']['field_geo_contact_title']['id'] = 'field_geo_contact_title';
$handler->display->display_options['fields']['field_geo_contact_title']['table'] = 'field_data_field_geo_contact_title';
$handler->display->display_options['fields']['field_geo_contact_title']['field'] = 'field_geo_contact_title';
/* Field: Content: GeoContact Is a State Contact */
$handler->display->display_options['fields']['field_geocontact_is_a_state_cont']['id'] = 'field_geocontact_is_a_state_cont';
$handler->display->display_options['fields']['field_geocontact_is_a_state_cont']['table'] = 'field_data_field_geocontact_is_a_state_cont';
$handler->display->display_options['fields']['field_geocontact_is_a_state_cont']['field'] = 'field_geocontact_is_a_state_cont';
$handler->display->display_options['fields']['field_geocontact_is_a_state_cont']['settings'] = array(
  'thousand_separator' => '',
  'prefix_suffix' => 1,
);
/* Field: Content: Is a State Contact? */
$handler->display->display_options['fields']['field_geo_is_a_state_contact']['id'] = 'field_geo_is_a_state_contact';
$handler->display->display_options['fields']['field_geo_is_a_state_contact']['table'] = 'field_data_field_geo_is_a_state_contact';
$handler->display->display_options['fields']['field_geo_is_a_state_contact']['field'] = 'field_geo_is_a_state_contact';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  8 => 0,
  6 => 0,
  5 => 0,
  10 => 0,
  11 => 0,
  4 => 0,
  7 => 0,
  3 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
);
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'geographic_contacts' => 'geographic_contacts',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/qa-geographic-contacts';
$translatables['qa_geographic_contacts'] = array(
  t('Master'),
  t('QA Geographic Contacts'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Displaying @start - @end of @total'),
  t('author'),
  t('Nid'),
  t('Title'),
  t('Published'),
  t('Post date'),
  t('Updated date'),
  t('URL alias'),
  t('Author'),
  t('Section'),
  t('Address'),
  t('Address3'),
  t('Body'),
  t('Geo Contact Active'),
  t('Geo Contact Agency'),
  t('Geo Contact Email'),
  t('Geo Contact First Name'),
  t('Geo Contact Last Name'),
  t('Geo Contact Latitude'),
  t('Geo Contact Longitude'),
  t('Geo Contact Number'),
  t('Geo Contact Office Fax'),
  t('Geo Contact Office Phone'),
  t('Geo Contact ProviderId'),
  t('Geo Contact State Name'),
  t('Geo Contact Title'),
  t('GeoContact Is a State Contact'),
  t('Is a State Contact?'),
  t('Page'),
);
