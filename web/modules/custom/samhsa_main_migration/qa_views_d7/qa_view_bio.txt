$view = new view();
$view->name = 'qa_biography_nodes';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'QA Biography Nodes';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'QA Biography Nodes';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '100';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'nid' => 'nid',
  'title' => 'title',
  'created' => 'created',
  'changed' => 'changed',
  'name' => 'name',
  'alias' => 'alias',
  'field_content_owner' => 'field_content_owner',
  'field_bio_image' => 'field_bio_image',
  'field_summary' => 'field_summary',
  'body' => 'body',
  'field_full_name' => 'field_full_name',
  'field_professional_title' => 'field_professional_title',
  'field_related_documents' => 'field_related_documents',
  'field_standards' => 'field_standards',
);
$handler->display->display_options['style_options']['default'] = 'nid';
$handler->display->display_options['style_options']['info'] = array(
  'nid' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'alias' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_content_owner' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_bio_image' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_summary' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'body' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_full_name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_professional_title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_related_documents' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_standards' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: Result summary */
$handler->display->display_options['header']['result']['id'] = 'result';
$handler->display->display_options['header']['result']['table'] = 'views';
$handler->display->display_options['header']['result']['field'] = 'result';
/* Relationship: Content: Author */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'long';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'long';
$handler->display->display_options['fields']['changed']['second_date_format'] = 'long';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
/* Field: Node: URL alias */
$handler->display->display_options['fields']['alias']['id'] = 'alias';
$handler->display->display_options['fields']['alias']['table'] = 'views_url_alias_node';
$handler->display->display_options['fields']['alias']['field'] = 'alias';
/* Field: Content: Content Owner */
$handler->display->display_options['fields']['field_content_owner']['id'] = 'field_content_owner';
$handler->display->display_options['fields']['field_content_owner']['table'] = 'field_data_field_content_owner';
$handler->display->display_options['fields']['field_content_owner']['field'] = 'field_content_owner';
$handler->display->display_options['fields']['field_content_owner']['delta_offset'] = '0';
/* Field: Content: Bio Image */
$handler->display->display_options['fields']['field_bio_image']['id'] = 'field_bio_image';
$handler->display->display_options['fields']['field_bio_image']['table'] = 'field_data_field_bio_image';
$handler->display->display_options['fields']['field_bio_image']['field'] = 'field_bio_image';
$handler->display->display_options['fields']['field_bio_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_bio_image']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: Workbench Access: Section */
$handler->display->display_options['fields']['section']['id'] = 'section';
$handler->display->display_options['fields']['section']['table'] = 'workbench_access';
$handler->display->display_options['fields']['section']['field'] = 'section';
/* Field: Content: Summary */
$handler->display->display_options['fields']['field_summary']['id'] = 'field_summary';
$handler->display->display_options['fields']['field_summary']['table'] = 'field_data_field_summary';
$handler->display->display_options['fields']['field_summary']['field'] = 'field_summary';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
/* Field: Content: Full Name */
$handler->display->display_options['fields']['field_full_name']['id'] = 'field_full_name';
$handler->display->display_options['fields']['field_full_name']['table'] = 'field_data_field_full_name';
$handler->display->display_options['fields']['field_full_name']['field'] = 'field_full_name';
/* Field: Content: Professional Title */
$handler->display->display_options['fields']['field_professional_title']['id'] = 'field_professional_title';
$handler->display->display_options['fields']['field_professional_title']['table'] = 'field_data_field_professional_title';
$handler->display->display_options['fields']['field_professional_title']['field'] = 'field_professional_title';
/* Field: Content: Related Documents */
$handler->display->display_options['fields']['field_related_documents']['id'] = 'field_related_documents';
$handler->display->display_options['fields']['field_related_documents']['table'] = 'field_data_field_related_documents';
$handler->display->display_options['fields']['field_related_documents']['field'] = 'field_related_documents';
$handler->display->display_options['fields']['field_related_documents']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_related_documents']['delta_offset'] = '0';
/* Field: Content: Standards Compliant? */
$handler->display->display_options['fields']['field_standards']['id'] = 'field_standards';
$handler->display->display_options['fields']['field_standards']['table'] = 'field_data_field_standards';
$handler->display->display_options['fields']['field_standards']['field'] = 'field_standards';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  8 => 0,
  6 => 0,
  5 => 0,
  10 => 0,
  11 => 0,
  4 => 0,
  7 => 0,
  3 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
);
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'biography' => 'biography',
);
/* Filter criterion: Content: Nid */
$handler->display->display_options['filters']['nid']['id'] = 'nid';
$handler->display->display_options['filters']['nid']['table'] = 'node';
$handler->display->display_options['filters']['nid']['field'] = 'nid';
$handler->display->display_options['filters']['nid']['exposed'] = TRUE;
$handler->display->display_options['filters']['nid']['expose']['operator_id'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['label'] = 'Nid';
$handler->display->display_options['filters']['nid']['expose']['operator'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['identifier'] = 'nid';
$handler->display->display_options['filters']['nid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  8 => 0,
  6 => 0,
  5 => 0,
  10 => 0,
  11 => 0,
  4 => 0,
  7 => 0,
  3 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
);
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  8 => 0,
  6 => 0,
  5 => 0,
  10 => 0,
  11 => 0,
  4 => 0,
  7 => 0,
  3 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/qa-biography-nodes';
$translatables['qa_biography_nodes'] = array(
  t('Master'),
  t('QA Biography Nodes'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Displaying @start - @end of @total'),
  t('author'),
  t('Nid'),
  t('Title'),
  t('Post date'),
  t('Updated date'),
  t('Name'),
  t('URL alias'),
  t('Content Owner'),
  t('Bio Image'),
  t('Section'),
  t('Summary'),
  t('Body'),
  t('Full Name'),
  t('Professional Title'),
  t('Related Documents'),
  t('Standards Compliant?'),
  t('Published'),
  t('Page'),
);
