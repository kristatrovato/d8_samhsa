$view = new view();
$view->name = 'qa_landing_page_nodes';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'QA Landing Page Nodes';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'QA Landing Page Nodes';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '100';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'nid' => 'nid',
  'title' => 'title',
  'status' => 'status',
  'name' => 'name',
  'created' => 'created',
  'changed' => 'changed',
  'alias' => 'alias',
  'field_banner' => 'field_banner',
  'field_long_title' => 'field_long_title',
  'field_summary' => 'field_summary',
  'body' => 'body',
  'field_content_owner' => 'field_content_owner',
  'field_en_espanol' => 'field_en_espanol',
  'field_in_english' => 'field_in_english',
  'field_standards' => 'field_standards',
  'field_audience_tags' => 'field_audience_tags',
  'field_issues_conditions_disorder' => 'field_issues_conditions_disorder',
  'field_location_tags' => 'field_location_tags',
  'field_population_groups_tags' => 'field_population_groups_tags',
  'field_professional_research_topi' => 'field_professional_research_topi',
  'field_substances_tags' => 'field_substances_tags',
  'field_treatment_prevention_and_r' => 'field_treatment_prevention_and_r',
);
$handler->display->display_options['style_options']['default'] = 'nid';
$handler->display->display_options['style_options']['info'] = array(
  'nid' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'alias' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_banner' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_long_title' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_summary' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'body' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_content_owner' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_en_espanol' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_in_english' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_standards' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_audience_tags' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_issues_conditions_disorder' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_location_tags' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_population_groups_tags' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_professional_research_topi' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_substances_tags' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_treatment_prevention_and_r' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Header: Global: Result summary */
$handler->display->display_options['header']['result']['id'] = 'result';
$handler->display->display_options['header']['result']['table'] = 'views';
$handler->display->display_options['header']['result']['field'] = 'result';
/* Relationship: Content: Author */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'node';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Field: Content: Published */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'Authored by';
/* Field: Content: Post date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['date_format'] = 'long';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
/* Field: Content: Updated date */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'node';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
/* Field: Node: URL alias */
$handler->display->display_options['fields']['alias']['id'] = 'alias';
$handler->display->display_options['fields']['alias']['table'] = 'views_url_alias_node';
$handler->display->display_options['fields']['alias']['field'] = 'alias';
/* Field: Workbench Access: Section */
$handler->display->display_options['fields']['section']['id'] = 'section';
$handler->display->display_options['fields']['section']['table'] = 'workbench_access';
$handler->display->display_options['fields']['section']['field'] = 'section';
/* Field: Content: Banner */
$handler->display->display_options['fields']['field_banner']['id'] = 'field_banner';
$handler->display->display_options['fields']['field_banner']['table'] = 'field_data_field_banner';
$handler->display->display_options['fields']['field_banner']['field'] = 'field_banner';
$handler->display->display_options['fields']['field_banner']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_banner']['settings'] = array(
  'image_style' => 'thumbnail',
  'image_link' => '',
);
/* Field: Content: Long Title */
$handler->display->display_options['fields']['field_long_title']['id'] = 'field_long_title';
$handler->display->display_options['fields']['field_long_title']['table'] = 'field_data_field_long_title';
$handler->display->display_options['fields']['field_long_title']['field'] = 'field_long_title';
/* Field: Content: Summary */
$handler->display->display_options['fields']['field_summary']['id'] = 'field_summary';
$handler->display->display_options['fields']['field_summary']['table'] = 'field_data_field_summary';
$handler->display->display_options['fields']['field_summary']['field'] = 'field_summary';
/* Field: Content: Body */
$handler->display->display_options['fields']['body']['id'] = 'body';
$handler->display->display_options['fields']['body']['table'] = 'field_data_body';
$handler->display->display_options['fields']['body']['field'] = 'body';
/* Field: Content: Content Owner */
$handler->display->display_options['fields']['field_content_owner']['id'] = 'field_content_owner';
$handler->display->display_options['fields']['field_content_owner']['table'] = 'field_data_field_content_owner';
$handler->display->display_options['fields']['field_content_owner']['field'] = 'field_content_owner';
$handler->display->display_options['fields']['field_content_owner']['delta_offset'] = '0';
/* Field: Content: En español */
$handler->display->display_options['fields']['field_en_espanol']['id'] = 'field_en_espanol';
$handler->display->display_options['fields']['field_en_espanol']['table'] = 'field_data_field_en_espanol';
$handler->display->display_options['fields']['field_en_espanol']['field'] = 'field_en_espanol';
$handler->display->display_options['fields']['field_en_espanol']['settings'] = array(
  'bypass_access' => 0,
  'link' => 0,
);
/* Field: Content: In English */
$handler->display->display_options['fields']['field_in_english']['id'] = 'field_in_english';
$handler->display->display_options['fields']['field_in_english']['table'] = 'field_data_field_in_english';
$handler->display->display_options['fields']['field_in_english']['field'] = 'field_in_english';
$handler->display->display_options['fields']['field_in_english']['settings'] = array(
  'bypass_access' => 0,
  'link' => 0,
);
/* Field: Content: Standards Compliant? */
$handler->display->display_options['fields']['field_standards']['id'] = 'field_standards';
$handler->display->display_options['fields']['field_standards']['table'] = 'field_data_field_standards';
$handler->display->display_options['fields']['field_standards']['field'] = 'field_standards';
/* Field: Content: Audience Tags */
$handler->display->display_options['fields']['field_audience_tags']['id'] = 'field_audience_tags';
$handler->display->display_options['fields']['field_audience_tags']['table'] = 'field_data_field_audience_tags';
$handler->display->display_options['fields']['field_audience_tags']['field'] = 'field_audience_tags';
$handler->display->display_options['fields']['field_audience_tags']['delta_offset'] = '0';
/* Field: Content: Issues, Conditions, & Disorders Tags */
$handler->display->display_options['fields']['field_issues_conditions_disorder']['id'] = 'field_issues_conditions_disorder';
$handler->display->display_options['fields']['field_issues_conditions_disorder']['table'] = 'field_data_field_issues_conditions_disorder';
$handler->display->display_options['fields']['field_issues_conditions_disorder']['field'] = 'field_issues_conditions_disorder';
$handler->display->display_options['fields']['field_issues_conditions_disorder']['delta_offset'] = '0';
/* Field: Content: Location Tags */
$handler->display->display_options['fields']['field_location_tags']['id'] = 'field_location_tags';
$handler->display->display_options['fields']['field_location_tags']['table'] = 'field_data_field_location_tags';
$handler->display->display_options['fields']['field_location_tags']['field'] = 'field_location_tags';
$handler->display->display_options['fields']['field_location_tags']['delta_offset'] = '0';
/* Field: Content: Population Groups Tags */
$handler->display->display_options['fields']['field_population_groups_tags']['id'] = 'field_population_groups_tags';
$handler->display->display_options['fields']['field_population_groups_tags']['table'] = 'field_data_field_population_groups_tags';
$handler->display->display_options['fields']['field_population_groups_tags']['field'] = 'field_population_groups_tags';
$handler->display->display_options['fields']['field_population_groups_tags']['delta_offset'] = '0';
/* Field: Content: Professional & Research Topics Tags */
$handler->display->display_options['fields']['field_professional_research_topi']['id'] = 'field_professional_research_topi';
$handler->display->display_options['fields']['field_professional_research_topi']['table'] = 'field_data_field_professional_research_topi';
$handler->display->display_options['fields']['field_professional_research_topi']['field'] = 'field_professional_research_topi';
$handler->display->display_options['fields']['field_professional_research_topi']['delta_offset'] = '0';
/* Field: Content: Substances Tags */
$handler->display->display_options['fields']['field_substances_tags']['id'] = 'field_substances_tags';
$handler->display->display_options['fields']['field_substances_tags']['table'] = 'field_data_field_substances_tags';
$handler->display->display_options['fields']['field_substances_tags']['field'] = 'field_substances_tags';
$handler->display->display_options['fields']['field_substances_tags']['delta_offset'] = '0';
/* Field: Content: Treatment Prevention and Recovery Tags */
$handler->display->display_options['fields']['field_treatment_prevention_and_r']['id'] = 'field_treatment_prevention_and_r';
$handler->display->display_options['fields']['field_treatment_prevention_and_r']['table'] = 'field_data_field_treatment_prevention_and_r';
$handler->display->display_options['fields']['field_treatment_prevention_and_r']['field'] = 'field_treatment_prevention_and_r';
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 'All';
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['exposed'] = TRUE;
$handler->display->display_options['filters']['status']['expose']['operator_id'] = '';
$handler->display->display_options['filters']['status']['expose']['label'] = 'Published';
$handler->display->display_options['filters']['status']['expose']['operator'] = 'status_op';
$handler->display->display_options['filters']['status']['expose']['identifier'] = 'status';
$handler->display->display_options['filters']['status']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  8 => 0,
  6 => 0,
  5 => 0,
  10 => 0,
  11 => 0,
  4 => 0,
  7 => 0,
  3 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
);
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'landing_page' => 'landing_page',
);
/* Filter criterion: Content: Nid */
$handler->display->display_options['filters']['nid']['id'] = 'nid';
$handler->display->display_options['filters']['nid']['table'] = 'node';
$handler->display->display_options['filters']['nid']['field'] = 'nid';
$handler->display->display_options['filters']['nid']['exposed'] = TRUE;
$handler->display->display_options['filters']['nid']['expose']['operator_id'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['label'] = 'Nid';
$handler->display->display_options['filters']['nid']['expose']['operator'] = 'nid_op';
$handler->display->display_options['filters']['nid']['expose']['identifier'] = 'nid';
$handler->display->display_options['filters']['nid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  8 => 0,
  6 => 0,
  5 => 0,
  10 => 0,
  11 => 0,
  4 => 0,
  7 => 0,
  3 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
);
/* Filter criterion: Content: Title */
$handler->display->display_options['filters']['title']['id'] = 'title';
$handler->display->display_options['filters']['title']['table'] = 'node';
$handler->display->display_options['filters']['title']['field'] = 'title';
$handler->display->display_options['filters']['title']['exposed'] = TRUE;
$handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
$handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
$handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
$handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  8 => 0,
  6 => 0,
  5 => 0,
  10 => 0,
  11 => 0,
  4 => 0,
  7 => 0,
  3 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
  15 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'admin/qa-landing-page-nodes';
$translatables['qa_landing_page_nodes'] = array(
  t('Master'),
  t('QA Landing Page Nodes'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Displaying @start - @end of @total'),
  t('author'),
  t('Nid'),
  t('Title'),
  t('Published'),
  t('Authored by'),
  t('Post date'),
  t('Updated date'),
  t('URL alias'),
  t('Section'),
  t('Banner'),
  t('Long Title'),
  t('Summary'),
  t('Body'),
  t('Content Owner'),
  t('En español'),
  t('In English'),
  t('Standards Compliant?'),
  t('Audience Tags'),
  t('Issues, Conditions, & Disorders Tags'),
  t('Location Tags'),
  t('Population Groups Tags'),
  t('Professional & Research Topics Tags'),
  t('Substances Tags'),
  t('Treatment Prevention and Recovery Tags'),
  t('Page'),
);
