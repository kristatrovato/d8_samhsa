<?php

namespace Drupal\samhsa_moderation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this site.
 */
class Settings extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'samhsa_moderation_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'samhsa_moderation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('samhsa_moderation.settings');
    $form['test'] = array(
        '#markup' => $this->t('Secondary database connection '),
    );
    // Do a pre-render test of the secondary database connection
    $test = _switch_to_d7();
    $test_output = $test ? "<strong>PASSED</strong>" : "<strong>FAILED</strong>";
    $form['test']['#markup'] .= $test_output . "<br /><br />";

    if ($test) {

        $user_list = _get_user_list();

        // Retrieve D7 content types
        $d7_types = _get_d7_types();

        // Return default database connection
        _switch_to_d8();

        // Retrieve D8 content types
        $d8_types = _get_d8_types();
        // Prepend a null-process option
        $d8_types = ['' => "Don't Process"] + $d8_types;

        // Provide a button to launch the migration
        $form['migrate'] = array(
            '#type' => 'button',
            '#value' => $this->t('Launch Migration'),
        );

        // Provide a button to reset the db
        $form['rewind'] = array(
            '#type' => 'button',
            '#value' => $this->t('WHOOPS! REWIND!'),
        );

        // Provide a default user selection
        $form['uid'] = array(
            '#type' => 'select',
            '#title' => $this->t('Default user'),
            '#description' => $this->t('The user to be listed as author if the original no longer exists'),
            '#options' => $user_list,
            '#default_value' => $config->get('uid'),
        );

        // Create the mapping wrapper
        $form['mapping'] = array(
            '#type' => 'details',
            '#title' => $this->t('Mapping'),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
            '#tree' => TRUE,
        );
        // Create the mapping fields
        foreach ($d7_types as $d7_type => $d7_name) {
            $form['mapping'][$d7_type] = array(
                '#type' => 'select',
                '#title' => $d7_name,
                '#default_value' => $config->get('mapping')[$d7_type],
                '#options' => $d8_types,
            );
        }

    } else {

        // Return default database connection
        _switch_to_d8();

    }

        return parent::buildForm($form, $form_state);
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('samhsa_moderation.settings');
    // Don't allow the triggering of migration unless something is mapped
      $trigger = $form_state->getTriggeringElement();
      if ( $trigger['#id'] == 'edit-migrate' ) {
        $value_found = FALSE;
        $mappings = [];
        $mapping = $form_state->getValue('mapping');
        foreach ( $mapping as $key => $value) {
            if ( $value != '' ) {
                $mappings[$key] = $value;
                $value_found = TRUE;
            }
        }
        if ( !$value_found ) {
            $form_state->setErrorByName('migrate', $this->t("Migration cannot be launched unless at least one content type is mapped"));
        }
        else {
          $user_list = _get_user_list();
          $stats =  _process(array_keys($mappings), $user_list, $config->get('uid'));
          $msg = "RECORDS PROCESSED: " . $stats['processed'] . " BYPASSED:" . $stats['bypassed'] . " NODES PROCESSED: " . $stats['nodes_processed'];
          drupal_set_message($msg);
        }
      }
      elseif ( $trigger['#id'] == 'edit-rewind' ) {
          _rewind();
          drupal_set_message('Moderation data removed', 'warning');
      }
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      // Retrieve the configuration
      $this->configFactory->getEditable('samhsa_moderation.settings')
      ->set('mapping', $form_state->getValue('mapping'))
      ->set('uid', $form_state->getValue('uid'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}