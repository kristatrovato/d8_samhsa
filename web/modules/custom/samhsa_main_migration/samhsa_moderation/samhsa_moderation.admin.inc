<?php
/**
 * Process moderation state migration. Called by admin form button click.
 */
/**
 * @param $mappings
 * @param $user_list
 * @param $default_uid
 * @return array
 */
function _process($mappings, $user_list, $default_uid) {
  _switch_to_d7();
  $states = _get_history($mappings);
  _switch_to_d8();

  $queue = NULL;
  $node = NULL;
  $stats = [
    'processed' => 0,
    'bypassed' => 0,
    'nodes_processed' => 0,
  ];

  foreach ($states as $bundle_type => $records) {
    $bypass = FALSE;
    foreach ($records as $stateObj) {
      if (sizeof($queue) > 0) {
        if ($queue[0]->nid == $stateObj->nid) {
          if ($queue[0]->published) {
            if ($stateObj->published) {
              $queue[0] = $stateObj;

            }
            elseif ($stateObj->is_current) {
              $queue[1] = $stateObj;

            }
          }
          elseif ($stateObj->is_current) {
            $queue[0] = $stateObj;

          }
        } // $queue[0]->nid = $stateObj->nid
        else {
          _create_cm_recs($queue, $node, $stats, $user_list, $default_uid);
          $queue = NULL;

          $node = Drupal\node\Entity\Node::load($stateObj->nid);
          $bypass = $node == NULL;

          // Bypass any nid that is not present in the system to account for any not migrated for some reason
          if (!$bypass) {
            $queue[0] = $stateObj;
          }
          else {
            // no-op since this nid doesn't exist
            ++$stats['bypassed'];
          } // bypass == true
        } // $queue[0]->nid == $stateObj->nid
      } // $sizeof($queue) > 0
      else {
        $node = Drupal\node\Entity\Node::load($stateObj->nid);
        if (is_object($node)) {//print "node exists\n\r";}else{//print "node doesn't exist\n\r";}
          $bypass = $node == NULL;

          // Bypass any nid that is not present in the system to account for any not migrated for some reason
          if (!$bypass) {

            $queue[0] = $stateObj;

          } // !$bypass
        } // sizeof($queue) > 0
      } // foreach $records
    } // foreach $states
  }

  return $stats;
}

/**
 * Switch the active database connection to the D7 database config in settings.php
 */
function _switch_to_d7() {
   return \Drupal\Core\Database\Database::setActiveConnection('external');
}

/**
 * Switch the active database connection back to the default (d8) in settings.php
 */
function _switch_to_d8() {
   return \Drupal\Core\Database\Database::setActiveConnection();
}

/**
 * Retrieve D7 content types
 */
function _get_d7_types() {
    $db = \Drupal\Core\Database\Database::getConnection();

    $query = $db->select('workbench_moderation_node_history', 'nh');
    $query->distinct();
    $query->fields('nh', array('nid'));
    $query->fields('n', array('type'));
    $query->leftJoin('node', 'n', 'nh.nid = n.nid');
    $query->leftJoin('node_type', 'nt', 'n.type = nt.type');
    $query->fields('nt', array('name'));
    $query->orderBy('n.type');
    $result = $query->execute();
    $table = $result->fetchAllKeyed(1,2);

    return $table;
}

function _get_d8_types() {
    $types = null;

    $list = \Drupal::entityTypeManager()
        ->getStorage('node_type')
        ->loadMultiple();

    foreach ($list as $type => $data) {
        $types[$data->id()] = $data->label();
    }

    return $types;
}

/**
 * Convert a table of d7 bundle => d8 bundle to an array of d7 bundle names for use in a mysql query
 * @param $mappings
 * @return string
 */
function _make_bundle_list($mappings) {
//    return array_keys($mappings);
    return "'" . implode("', '", $mappings) . "'";
}

/**
 * @param $bundles
 * @return array
 */
function _get_history($bundles) {

    $db = \Drupal\Core\Database\Database::getConnection();
    $table = [];
    $query = $db->select('workbench_moderation_node_history', 'nh');
    $query->fields('nh', array('nid', 'from_state', 'uid', 'state', 'published', 'is_current'));
    $query->fields('n', array('type'));
    $query->leftJoin('node', 'n', 'nh.nid = n.nid');
    $query->condition('type', $bundles, 'in');
    $result = $query->execute();
    foreach ($result as $rec) {
        $table[$rec->type][] = $rec;
    }
    return $table;
}

/**
 * @param $state_obj
 * @param $node
 * @param $hold_published
 *
 */
function _create_cm_recs($queue, $node, &$stats, $user_list, $default_uid) {
    // if the record to be processed is not published, process the held published record if there is one
    if (!$queue[0]->published && is_object($queue[1])) {

      // published record
      $id = _create_content_moderation_state($node);
      $rid = _create_content_moderation_state_revision($node, $id);
      _create_content_moderation_state_field_data($queue[1], $node, $id, $rid, $user_list, $default_uid);
      _create_content_moderation_state_field_revision($queue[1], $node, $id, $rid, $user_list, $default_uid);
      ++$stats['processed'];
      ++$stats['nodes_processed'];

      // current record
      $rid = _create_content_moderation_state_revision($node, $id);
      _update_state_with_revision_id($id, $rid);
      _create_content_moderation_state_field_revision($queue[0], $node, $id, $rid, $user_list, $default_uid);
      ++$stats['processed'];
    }
    else {
      // Current record is published or is not published but there is no published one for this nid
      $id = _create_content_moderation_state($node);
      $rid = _create_content_moderation_state_revision($node, $id);
      _update_state_with_revision_id($id, $rid);
      _create_content_moderation_state_field_data($queue[0], $node, $id, $rid, $user_list, $default_uid);
      _create_content_moderation_state_field_revision($queue[0], $node, $id, $rid, $user_list, $default_uid);
      ++$stats['nodes_processed'];
      ++$stats['processed'];
    }
}

/**
 * @param $hist
 * @param \Drupal\node\Entity\Node $node
 * @return array
 */
function _create_content_moderation_state(Drupal\node\Entity\Node $node) {
    $rec = [
        'id' => null,
        'revision_id' => null, // this will be filled in via update after the value is assigned
        'uuid' =>   $node->uuid(),
        'langcode' => $node->langcode->value,
    ];

    $db = \Drupal\Core\Database\Database::getConnection();
    $id = $db->insert('content_moderation_state')
        ->fields( $rec )
        ->execute();
  return $id;
}

/**
 * @param $hist
 * @param \Drupal\node\Entity\Node $node
 * @param $id
 * @return array
 */
function _create_content_moderation_state_revision(Drupal\node\Entity\Node $node, $id) {
    $rec = [
        'id' => $id,
        'revision_id' => null, // this will be filled in via update after the value is assigned
        'langcode' => $node->langcode->value,
        'revision_default' => 1,
    ];

    $db = \Drupal\Core\Database\Database::getConnection();
    $rid = $db->insert('content_moderation_state_revision')
        ->fields( $rec )
        ->execute();

    return $rid;
}

/**
 * @param $hist
 * @param $node
 * @param $id
 * @param $rid
 */
function _create_content_moderation_state_field_data($hist, $node, $id, $rid, $user_list, $default_uid) {
  $user =  (isset($user_list[$hist->uid])) ? $hist->uid : $default_uid;
  $rec = [
    'id' => $id,
    'revision_id' => $rid, // this will be filled in via update after the value is assigned
    'langcode' => $node->langcode->value,
    'uid' =>   $user,
    'workflow' => 'samhsa_main_custom',
    'moderation_state' => $hist->state,
    'content_entity_type_id' => 'node',
    'content_entity_id' => $hist->nid,
    'content_entity_revision_id' => $hist->vid,
    'default_langcode' => 1,
    'revision_translation_affected' => TRUE,
  ];

  $db = \Drupal\Core\Database\Database::getConnection();
  $db->insert('content_moderation_state_field_data')
    ->fields( $rec )
    ->execute();
}

/**
 * @param $hist
 * @param $node
 * @param $id
 * @param $rid
 */
function _create_content_moderation_state_field_revision($hist, $node, $id, $rid, $user_list, $default_uid) {
  $user =  (isset($user_list[$hist->uid])) ? $hist->uid : $default_uid;
  $rec = [
    'id' => $id,
    'revision_id' => $rid, // this will be filled in via update after the value is assigned
    'langcode' => $node->langcode->value,
    'uid' =>   $user,
    'workflow' => 'samhsa_main_custom',
    'moderation_state' => $hist->state,
    'content_entity_type_id' => 'node',
    'content_entity_id' => $hist->nid,
    'content_entity_revision_id' => $hist->vid,
    'default_langcode' => 1,
    'revision_translation_affected' => TRUE,
  ];

  $db = \Drupal\Core\Database\Database::getConnection();
  $db->insert('content_moderation_state_field_revision')
    ->fields( $rec )
    ->execute();
}

/**
 * @param $id
 * @param $rid
 */
function _update_state_with_revision_id($id, $rid) {
    $db = \Drupal\Core\Database\Database::getConnection();
    $db->update('content_moderation_state')
        ->fields([
          'revision_id' => $rid,
        ])
        ->condition('id', $id, '=')
        ->execute();
}

function _rewind() {
  $db = \Drupal\Core\Database\Database::getConnection();
  $db->delete('content_moderation_state_field_revision')->execute();
  $db->delete('content_moderation_state_field_data')->execute();
  $db->delete('content_moderation_state_revision')->execute();
  $db->delete('content_moderation_state')->execute();
}

function _get_user_list() {
  // Retrieve users
  $users =  \Drupal\user\Entity\User::loadMultiple();
  $user_list = [];
  foreach ($users as $uid => $user) {
    $user_list[$uid] = $user->getUsername() . " [$uid]";
  }
  $user_list[0] = 'Anonymous User [0]';

  return $user_list;
}