#!/bin/bash

drush migrate-import upgrade_d7_taxonomy_vocabulary

drush migrate-import upgrade_d7_taxonomy_term_ebp_audience --update

drush migrate-import upgrade_d7_taxonomy_term_ebp_conditions --update

drush migrate-import upgrade_d7_taxonomy_term_ebp_population --update

drush migrate-import upgrade_d7_taxonomy_term_ebp_portal --update

drush migrate-import upgrade_d7_taxonomy_term_ebp_resource_type --update

drush migrate-import upgrade_d7_taxonomy_term_dbhis_collections --update

drush migrate-import upgrade_d7_taxonomy_term_audience --update

drush migrate-import upgrade_d7_taxonomy_term_treatment_prevention_recovery_tags --update

