#!/bin/bash

drush migrate-import upgrade_d7_node_budget_section_csv --update
drush migrate-import upgrade_d7_node_ebp_resource_section_csv --update
drush migrate-import upgrade_d7_node_generic_org_structure_section_csv --update
drush migrate-import upgrade_d7_node_generic_section_acws_csv --update
drush migrate-import upgrade_d7_node_geographic_contacts_section_csv --update
drush migrate-import upgrade_d7_node_homepage_item_section_csv --update
drush migrate-import upgrade_d7_node_hpr_resource_section_csv --update
drush migrate-import upgrade_d7_node_hpr_video_section_csv --update
drush migrate-import upgrade_d7_node_infographic_section_csv --update
drush migrate-import upgrade_d7_node_landing_page_section_csv --update
drush migrate-import upgrade_d7_node_landing_programs_campaigns_section_csv --update
drush migrate-import upgrade_d7_node_landing_programs_campaigns_section_spanish
drush migrate-import upgrade_d7_node_meeting_section_csv --update
drush migrate-import upgrade_d7_node_meeting_section_acws_csv --update
drush migrate-import upgrade_d7_node_news_section_csv --update
drush migrate-import upgrade_d7_node_news_link_section_csv --update
drush migrate-import upgrade_d7_node_slide_section_csv --update
drush migrate-import upgrade_d7_node_biography_section_csv --update
drush migrate-import upgrade_d7_node_fep_program_section_csv --update
drush migrate-import upgrade_d7_node_rfa_section_csv --update
drush migrate-import upgrade_d7_node_generic_section_csv --update
drush migrate-import upgrade_d7_node_wellness_event_section_csv --update
