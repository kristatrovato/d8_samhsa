#!/bin/bash

drush migrate-import upgrade_d7_node_generic_org_structure --update

drush migrate-import upgrade_d7_node_revision_generic_org_structure --update

drush migrate-import upgrade_d7_node_generic_org_structure_update --update
