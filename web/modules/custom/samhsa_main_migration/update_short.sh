#!/bin/bash

drush sql-drop --yes
drush sql-cli < /var/www/html/d8_samhsa/_backups/samhsa_latest.sql
drush cr
