<?php
/**
 * Created by PhpStorm.
 * User: jeff.greenberg
 * Date: 5/17/2019
 * Time: 9:27 AM
 */
namespace Drupal\samhsa_blog_utility\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure block feed source
 */
class Settings extends ConfigFormBase {
/**
 * {@inheritdoc}
 */
    public function getFormId() {
        return 'samhsa_blog_utility_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'samhsa_blog_utility.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('samhsa_blog_utility.settings');

    $form['samhsa_blog_feed_url'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('SAMHSA Blog Feed URL'),
        '#description' => $this->t('URL of the feed, e.g. <em>http://blog.samhsa.gov/feed/</em>'),
        '#default_value' => $config->get('samhsa_blog_feed_url'),
        '#placeholder' => 'http://',
    );
    $form['rss_blog_feed_url'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('RSS Blog Feed URL'),
        '#description' => $this->t('URL of the feed, e.g. <em>https://www.healthcare.gov/api/blog.json</em>'),
        '#default_value' => $config->get('rss_blog_feed_url'),
        '#placeholder' => 'https://',
    );

    return parent::buildForm($form, $form_state);
  }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        // Retrieve the configuration
        $this->configFactory->getEditable('samhsa_blog_utility.settings')
            // Set the submitted configuration setting
            ->set('samhsa_blog_feed_url', $form_state->getValue('samhsa_blog_feed_url'))
            ->set('rss_blog_feed_url', $form_state->getValue('rss_blog_feed_url'))
            ->save();

        parent::submitForm($form, $form_state);
    }
}