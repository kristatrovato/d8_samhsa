<?php

namespace Drupal\samhsa_blog_utility\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\UncacheableDependencyTrait;

/**
 * Provides a SAMHSA blog block.
 *
 * @Block(
 *   id = "samhsa_blog",
 *   admin_label = @Translation("SAMHSA Blog"),
 *   category = "SAMHSA",
 * )
 */
class SamhsaBlog extends BlockBase
{
    use UncacheableDependencyTrait;

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        \Drupal::service('page_cache_kill_switch')->trigger();

        $config = \Drupal::configFactory()->get('samhsa_blog_utility.settings');
        $url = $config->get('feed_url');
        if ($url) {
            $var = $this->markup($url);
            return [
                '#theme' => 'samhsa_blog',
                '#title' => 'SAMHSA Blog',
                '#cache' => ['contexts' => ['url.path', 'url.query_args']],
                '#atitle' => $var['title'],
                '#description' => $var['description'],
                '#author' => $var['author'],
                '#path' => $var['path'],
                '#date' => $var['date'],
                '#link' => $var['link'],
            ];
        }
        else
        {
            return [];
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function blockAccess(AccountInterface $account)
    {
        return AccessResult::allowedIfHasPermission($account, 'access content');
    }

    private function markup($url)
    {
        {
            $xmlData = file_get_contents($url);
            $data = simplexml_load_string($xmlData);

            $des = (string)$data->channel->item->description;
            $title = (string)$data->channel->item->title;
            $link = (string)$data->channel->item->link;
            $dat1 = (string)$data->channel->item->pubDate;

            $dat = substr($dat1, 0, 16);

            $dess = explode("\n", $des);

//CHECK TO SEE IF AUTHOR INFORMATION IS VALID
            if (strpos($dess[0], 'By') !== false) {
                $author = $dess[0];
                $description = $dess[1];
                $date = $dat . '&nbsp;&nbsp;&nbsp;&nbsp';
            } elseif (strpos(isset($dess[1]) ? $dess[1] : "", 'By') !== false) {
                $author = $dess[0] . ' &#8226; ' . $dess[1];
                $description = $dess[2];
                $date = '';
            } else {
                $author = 'By: SAMHSA';
                $description = $dess[0];
                $date = $dat . '&nbsp;&nbsp;&nbsp;&nbsp';
            }

            $module_handler = \Drupal::service('module_handler');
            $path = $module_handler->getModule('samhsa_blog_utility')->getPath();
        }

        return [
                'link'  => $link,
                'title' => $title,
                'date'  => $date,
                'author' => $author,
                'description' => $description,
                'path' => $path
        ];
    }
}