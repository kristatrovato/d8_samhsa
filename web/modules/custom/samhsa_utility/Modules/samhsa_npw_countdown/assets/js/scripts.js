/*

    About this file:
    ================
    This file contains the Javascript required to perform the countdown on the National Prevention Month 2015 countdown widget.

    The code is encapsulated in the "countdownWidget" variable to avoid conflicts with other code.
    The widget's variables are exposed as much as possible to allow troubleshooting as required.

    If a conflict exists, simply change the prefix. Be certain to also update the related HTML and Javascript
    so that the same prefixes are used everywhere.

    Implementation instructions:
    ============================

    This widget requires a current version of jQuery. It was written and tested for jQuery 1.7.2 which was the version
    operating on beta.samhsa.gov when the code was written in September 2013.

    Drop this snippet of code where the pages with the countdown widget can see them. You can:
      - include the JS inline between <script>...</script> tags
      - add the JS in one of your existing .js files
      - include the countdown.js alone and call it using <script src=".../countdown.js"></script>


    Note on fallbacks (graceful degradation):
    =========================================

    In the instance where the JS is not available, none of the language regarding the time remaining will
    appear and the page will display simply:
    
        National Prevention Day

        Starting May 17, 2015

    In the instance where neither the style nor the JS is available, none of the countdown will appear and the page
    will display one line as a well-formed sentence.
    
        National Prevention Day Starting May 8, 2014

*/

    var countdownWidget = {};
(function ($) {
    $(document).ready( function() {
        //get date parts from drupalSettings
        var m = drupalSettings.samhsaNpwCountdown.theme.parts.m;
        var d = drupalSettings.samhsaNpwCountdown.theme.parts.d;
        var y = drupalSettings.samhsaNpwCountdown.theme.parts.y;
        countdownWidget = {

            //basic settings
            enabled: true, //use this settings to easily turn functionality on or off as required
            dDay: new Date(y,m,d), //January=0, so May=4 not 5
            paused: false,
            hidden: true,
            timeRemaining: new Array(),

            //Primary HTML element we will be manipulating
            $obj: $(".countdown-widget"),

            //The pause/play control that we'll add to the widget
            $control: $("<div class='countdown-control pause'>Pause/Play Countdown</div>"),

            //Initializes the widget
            init: function() {
                var self = countdownWidget;
                //If there is no countdown HTML currently on the page, past D-Day or widget disabled, do nothing
                if(self.$obj.length == 0 || self.dDay < (new Date()) || !self.enabled) {
                    self.paused = true;
                    self.hidden = true;
                }
                //If the widget is there, relevant and enabled, then initialize it
                else {
                    //Append the control animation and configure it to launch the toggle sequence
                    this.$control.click(this.toggleAnimation).prependTo(this.$obj.find(".countdown-time-remaining"));

                    //Add in semantic content that is only relevant if we are using the widget
                    this.$obj.find(".countdown-comma").text(", ");
                    this.$obj.find(".countdown-time-until").text("until");

                    //Configure ready-to-access pointers to elements that will be updated with countdown function
                    $.extend(this, {
                        $days: this.$obj.find(".days"),
                        $hours: this.$obj.find(".hours"),
                        $minutes: this.$obj.find(".minutes"),
                        $seconds: this.$obj.find(".seconds"),
                    });
                    this.countdown();
                }
            },

            //Primary function, runs every second (1000 milliseconds) and recalculates and rewrites time remaining
            countdown: function countdown() {
                var self = countdownWidget;
                if(!self.paused && self.enabled && self.$obj.length) {
                    var now = new Date();
                    var dDiff = new Date(self.dDay.getTime() - now.getTime());
                    self.timeRemaining = {
                        'days': Math.floor(dDiff.getTime() / 86400000),
                        'hours': dDiff.getHours(),
                        'minutes': dDiff.getMinutes(),
                        'seconds': dDiff.getSeconds()
                    };
                    self.$days.children('.countdown-time-num').text(self.timeRemaining.days);
                    self.$days.children('.countdown-time-word').text("day"+(self.timeRemaining.days == 1 ? "": "s"));

                    self.$hours.children('.countdown-time-num').text(self.timeRemaining.hours);
                    self.$hours.children('.countdown-time-word').text("hour"+(self.timeRemaining.hours == 1 ? "": "s"));

                    self.$minutes.children('.countdown-time-num').text(self.timeRemaining.minutes);
                    self.$minutes.children('.countdown-time-word').text("minute"+(self.timeRemaining.minutes == 1 ? "": "s"));

                    self.$seconds.children('.countdown-time-num').text(self.timeRemaining.seconds).toggleClass("lt10",(self.timeRemaining.seconds < 10));
                    self.$seconds.children('.countdown-time-word').text("second"+(self.timeRemaining.seconds == 1 ? "": "s"));

                    setTimeout(self.countdown, 1000);

                    //If first time running then display
                    if(self.hidden) {
                        this.$obj.find(".countdown-time-remaining").slideDown();
                        self.hidden = false;
                    }
                }
            },

            //Pauses or plays the coundown
            toggleAnimation: function() {
                var self = countdownWidget;
                self.paused = !self.paused;
                self.countdown();
                self.$control.toggleClass("pause");
            }
        };

        //Run the init function
        countdownWidget.init();
    });
}(jQuery));