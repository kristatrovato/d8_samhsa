// JavaScript Document
<!-- countdown widget Javascript. Can be placed in another file that gets loaded on the same page as where the widget resides -->
<!--
var countdownWidget = {};
$(document).ready(function () {
    countdownWidget = {
        //basic settings
        enabled: true, //use this settings to easily turn functionality on or off as required
        dDay: new Date(2014, 4, 18, 0, 0, 0), //January=0, so May=4 not 5
        paused: false,
        hidden: true,
        timeRemaining: new Array(),
        //Primary HTML element we will be manipulating
        $obj: $(".countdown-widget"),
        //The pause/play control that we'll add to the widget
        $control: $("<div class='countdown-control pause'>Pause/Play Countdown</div>"),
        //Initializes the widget
        init: function () {
            var self = countdownWidget;
            //If there is no countdown HTML currently on the page, past D-Day or widget disabled, do nothing
            if (self.$obj.length == 0 || self.dDay < (new Date()) || !self.enabled) {
                self.paused = true;
                self.hidden = true;
            }
            //If the widget is there, relevant and enabled, then initialize it
            else {
                //Append the control animation and configure it to launch the toggle sequence
                this.$control.click(this.toggleAnimation).prependTo(this.$obj.find(".countdown-time-remaining"));
                //Add in semantic content that is only relevant if we are using the widget
                this.$obj.find(".countdown-comma").text(", ");
                this.$obj.find(".countdown-until").text("until");
                //Configure ready-to-access pointers to elements that will be updated with countdown function
                $.extend(this, {
                    $days: this.$obj.find(".days"),
                    $hours: this.$obj.find(".hours"),
                    $minutes: this.$obj.find(".minutes"),
                    $seconds: this.$obj.find(".seconds"),
                });
                this.countdown();
            }
        },
        //Primary function, runs every second (1000 milliseconds) and recalculates and rewrites time remaining
        countdown: function countdown() {
            var self = countdownWidget;
            if (!self.paused && self.enabled && self.$obj.length) {
                var now = new Date();
                var dDiff = new Date(self.dDay.getTime() - now.getTime());

                var daysLeft = Math.floor(dDiff.getTime() / 86400000);

                var tmp = Math.floor(dDiff.getTime()) % 86400000;

                hoursLeft = tmp/3600000;

                tmp = tmp % 3600000;

                minutesLeft = tmp/60000;

                var secondsLeft = tmp % 60000;

                secondsLeft = secondsLeft/1000;

                hoursLeft = Math.floor(hoursLeft);
                minutesLeft = Math.floor(minutesLeft);
                secondsLeft = Math.floor(secondsLeft);
                self.timeRemaining = {
                    'days': daysLeft,
                    'hours': hoursLeft,
                    'minutes': minutesLeft,
                    'seconds': secondsLeft
                };
                self.$days.children('.countdown-time-num').text(self.timeRemaining.days);
                self.$days.children('.countdown-time-word').text("day" + (self.timeRemaining.days == 1 ? "" : "s"));
                self.$hours.children('.countdown-time-num').text(self.timeRemaining.hours);
                self.$hours.children('.countdown-time-word').text("hour" + (self.timeRemaining.hours == 1 ? "" : "s"));
                self.$minutes.children('.countdown-time-num').text(self.timeRemaining.minutes);
                self.$minutes.children('.countdown-time-word').text("minute" + (self.timeRemaining.minutes == 1 ? "" : "s"));
                self.$seconds.children('.countdown-time-num').text(self.timeRemaining.seconds).toggleClass("lt10", (self.timeRemaining.seconds < 10));
                self.$seconds.children('.countdown-time-word').text("second" + (self.timeRemaining.seconds == 1 ? "" : "s"));
                setTimeout(self.countdown, 1000);
                //If first time running then display
                if (self.hidden) {
                    this.$obj.find(".countdown-time-remaining").slideDown();
                    self.hidden = false;
                }
            }
        },
        //Pauses or plays the coundown
        toggleAnimation: function () {
            var self = countdownWidget;
            self.paused = !self.paused;
            self.countdown();
            self.$control.toggleClass("pause");
        }
    };
    //Run the init function
    countdownWidget.init();
});
//-->