<?php

namespace Drupal\samhsa_npw_countdown\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\UncacheableDependencyTrait;

/**
 * Provides a Spanish SAMHSA NPW countdown block.
 *
 * @Block(
 *   id = "samhsa_npw_countdown_EN",
 *   admin_label = @Translation("SAMHSA NPW Countdown"),
 *   category = "SAMHSA",
 * )
 */
class SamhsaNpwCountdownEN extends BlockBase
{
    use UncacheableDependencyTrait;

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        \Drupal::service('page_cache_kill_switch')->trigger();

        $config = \Drupal::configFactory()->get('samhsa_npw_countdown.settings');
        $date = $config->get('samhsa_npw_date');
        $year = $config->get('samhsa_npw_year');

        return [
            '#theme' => 'samhsa_npw_countdown_en',
            '#atitle' => 'NPW Countdown',
//                '#cache' => ['contexts' => ['url.path', 'url.query_args']],
            '#date' => $date,
            '#year' => $year,
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function blockAccess(AccountInterface $account)
    {
        return AccessResult::allowedIfHasPermission($account, 'access content');
    }

}