<?php
/**
 * Created by PhpStorm.
 * User: jeff.greenberg
 * Date: 5/28/2019
 * Time: 8:27 AM
 */
namespace Drupal\samhsa_npw_countdown\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure block feed source
 */
class Settings extends ConfigFormBase {
/**
 * {@inheritdoc}
 */
    public function getFormId() {
        return 'samhsa_npw_countdown_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'samhsa_npw_countdown.settings',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('samhsa_npw_countdown.settings');

    $form['samhsa_npw_date'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('English NPW Week start date'),
        '#description' => 'English date string of the NPW Week starting date.',
        '#default_value' => $config->get('samhsa_npw_date'),
        '#placeholder' => 'January 1, 2018',
    );
    $form['samhsa_npw_date_es'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Spanish NPW Week start date'),
        '#description' => 'Spanish date string of the NPW Week starting date.',
        '#default_value' => $config->get('samhsa_npw_date_es'),
        '#placeholder' => '1 de Enario de 2018',
    );
    $form['samhsa_npw_year'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Year of NPW Week'),
        '#default_value' => $config->get('samhsa_npw_year'),
        '#placeholder' => '2018',
    );

    return parent::buildForm($form, $form_state);
  }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        // Retrieve the configuration
        $this->configFactory->getEditable('samhsa_npw_countdown.settings')
            // Set the submitted configuration setting
            ->set('samhsa_npw_date', $form_state->getValue('samhsa_npw_date'))
            ->set('samhsa_npw_date_es', $form_state->getValue('samhsa_npw_date_es'))
            ->set('samhsa_npw_year', $form_state->getValue('samhsa_npw_year'))
            ->save();

        parent::submitForm($form, $form_state);
    }
}