<?php

namespace Drupal\samhsa_grants_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SamhsaMapUs' block.
 *
 * @Block(
 *  id = "samhsa_map_us",
 *  admin_label = @Translation("Samhsa map US"),
 * )
 */
class SamhsaMapUs extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        
        return [ '#markup' => $this->_grants_map_markup(), '#cache' => ['max-age' => 0,] ];
    }
  
  private function _grants_map_markup() {
      
      global $base_url;
      
      if (isset($_GET['year'])) {
          $year = $_GET['year'];
      }else{ $year = '2018'; }
      
      $MapString = '<p><center><h3>Fiscal Year: '.$year.'</h3></center>' .
          '<img alt="Map of the United States" height="495" src="' . $base_url .
          '/sites/default/files/images/800px-map_of_usa_updated.png" usemap="#usStates" width="800" />' .
          '<map id="usStates" name="usStates">' .
          '<area alt="Alabama" coords="537,310,556,377,518,382,518,395,506,391,504,312" href="'.$base_url.'/grants-awards-by-state/AL/'.$year.'" shape="poly" title="Alabama" />' .
          '<area alt="Alaska" coords="127,381,128,451,148,457,171,481,171,491,153,491,132,461,98,456,83,473,10,482,55,456,34,431,43,391,60,375,89,365" href="'.$base_url.'/grants-awards-by-state/AK/'.$year.'" shape="poly" title="Alaska" />' .
          '<area alt="Arizona" coords="110,330,159,362,191,368,204,271,134,258,131,272,123,272,117,290,126,304" href="'.$base_url.'/grants-awards-by-state/AZ/'.$year.'" shape="poly" title="Arizona" />' .
          '<area alt="Arkansas" coords="409,289,408,337,416,350,460,350,472,311,479,295,467,290" href="'.$base_url.'/grants-awards-by-state/AR/'.$year.'" shape="poly" title="Arkansas" />' .
          '<area alt="California" coords="23,137,76,151,61,203,117,283,115,289,124,303,112,329,76,325,34,279,11,162" href="'.$base_url.'/grants-awards-by-state/CA/'.$year.'" shape="poly" title="California" />' .
          '<area alt="Colorado" coords="214,196,308,207,304,276,206,267" href="'.$base_url.'/grants-awards-by-state/CO/'.$year.'" shape="poly" title="Colorado" />' .
          '<area alt="Connecticut" coords="683,154,683,172,705,163,703,150" href="'.$base_url.'/grants-awards-by-state/CT/'.$year.'" shape="poly" title="Connecticut" />' .
          '<area alt="Delaware" coords="663,199,667,225,678,225,674,213" href="'.$base_url.'/grants-awards-by-state/DE/'.$year.'" shape="poly" title="Delaware" />' .
          '<area alt="Florida" coords="610,378,648,455,632,486,617,487,596,434,574,394,553,405,521,393,521,384,556,379,556,384" href="'.$base_url.'/grants-awards-by-state/FL/'.$year.'" shape="poly" title="Florida" />' .
          '<area alt="Georgia" coords="537,307,572,304,614,348,609,380,557,381" href="'.$base_url.'/grants-awards-by-state/GA/'.$year.'" shape="poly" title="Georgia" />' .
          '<area alt="Hawaii" coords="183,423,180,461,216,491,283,492,286,465,244,424" href="'.$base_url.'/grants-awards-by-state/HI/'.$year.'" shape="poly" title="Hawaii" />' .
          '<area alt="Idaho" coords="141,40,128,89,134,97,121,117,114,159,189,172,197,131,172,128,167,106,158,109,160,88,146,60,152,42" href="'.$base_url.'/grants-awards-by-state/ID/'.$year.'" shape="poly" title="Idaho" />' .
          '<area alt="Illinois" coords="463,184,450,219,470,243,467,254,486,275,496,274,505,243,501,189,493,174,461,179" href="'.$base_url.'/grants-awards-by-state/IL/'.$year.'" shape="poly" title="Illinois" />' .
          '<area alt="Indiana" coords="503,191,502,260,527,254,542,237,537,185" href="'.$base_url.'/grants-awards-by-state/IN/'.$year.'" shape="poly" title="Indiana" />' .
          '<area alt="Iowa" coords="385,164,379,179,391,215,446,215,454,204,461,188,451,165" href="'.$base_url.'/grants-awards-by-state/IA/'.$year.'" shape="poly" title="Iowa" />' .
          '<area alt="Kansas" coords="307,224,301,278,407,280,407,243,401,238,404,231,393,224" href="'.$base_url.'/grants-awards-by-state/KS/'.$year.'" shape="poly" title="Kansas" />' .
          '<area alt="Kentucky" coords="485,286,565,275,582,259,569,241,544,234,528,258,502,261" href="'.$base_url.'/grants-awards-by-state/KY/'.$year.'" shape="poly" title="Kentucky" />' .
          '<area alt="Louisiana" coords="421,407,426,382,416,367,418,351,461,351,463,363,456,385,479,385,488,396,495,416,456,421" href="'.$base_url.'/grants-awards-by-state/LA/'.$year.'" shape="poly" title="Louisiana" />' .
          '<area alt="Maine" coords="698,93,709,124,713,131,748,88,740,74,728,50,708,47" href="'.$base_url.'/grants-awards-by-state/ME/'.$year.'" shape="poly" title="Maine" />' .
          '<area alt="Maryland" coords="611,211,611,219,631,212,649,222,648,229,658,232,666,247,677,224,665,224,662,202" href="'.$base_url.'/grants-awards-by-state/MD/'.$year.'" shape="poly" title="Maryland" />' .
          '<area alt="Massachusetts" coords="681,142,682,155,708,150,715,156,733,158,733,143,713,134" href="'.$base_url.'/grants-awards-by-state/MA/'.$year.'" shape="poly" title="Massachusetts" />' .
          '<area alt="Michigan" coords="454,95,463,109,497,124,511,187,554,183,562,159,540,107,485,71" href="'.$base_url.'/grants-awards-by-state/MI/'.$year.'" shape="poly" title="Michigan" />' .
          '<area alt="Minnesota" coords="374,66,380,119,378,123,385,130,385,164,451,164,451,155,428,140,426,125,434,105,464,80,403,72,399,59" href="'.$base_url.'/grants-awards-by-state/MN/'.$year.'" shape="poly" title="Minnesota" />' .
          '<area alt="Mississippi" coords="474,314,503,311,506,394,488,395,478,383,456,383,464,356,463,346" href="'.$base_url.'/grants-awards-by-state/MS/'.$year.'" shape="poly" title="Mississippi" />' .
          '<area alt="Missouri" coords="392,216,393,224,406,240,407,288,472,287,468,295,476,295,486,279,466,254,469,245,447,218" href="'.$base_url.'/grants-awards-by-state/MO/'.$year.'" shape="poly" title="Missouri" />' .
          '<area alt="Montana" coords="154,41,149,58,162,87,159,106,167,103,174,127,196,128,198,123,285,132,293,63" href="'.$base_url.'/grants-awards-by-state/MT/'.$year.'" shape="poly" title="Montana" />' .
          '<area alt="Nebraska" coords="284,169,281,204,307,206,307,221,396,224,384,181,355,174" href="'.$base_url.'/grants-awards-by-state/NE/'.$year.'" shape="poly" title="Nebraska" />' .
          '<area alt="Nevada" coords="77,152,150,167,130,270,122,270,118,285,61,204" href="'.$base_url.'/grants-awards-by-state/NV/'.$year.'" shape="poly" title="Nevada" />' .
          '<area alt="New Hampshire" coords="692,95,688,140,711,134,697,91" href="'.$base_url.'/grants-awards-by-state/NH/'.$year.'" shape="poly" title="New Hampshire" />' .
          '<area alt="New Jersey" coords="668,173,667,188,673,193,665,200,675,212,687,187,680,182,680,176" href="'.$base_url.'/grants-awards-by-state/NJ/'.$year.'" shape="poly" title="New Jersey" />' .
          '<area alt="New Mexico" coords="204,267,290,275,281,364,204,360,202,370,189,366" href="'.$base_url.'/grants-awards-by-state/NM/'.$year.'" shape="poly" title="New Mexico" />' .
          '<area alt="New York" coords="607,148,597,171,658,159,667,170,680,174,680,180,687,184,707,169,685,171,682,154,682,139,670,103,648,107,629,139" href="'.$base_url.'/grants-awards-by-state/NY/'.$year.'" shape="poly" title="New York" />' .
          '<area alt="North Carolina" coords="589,274,556,305,597,298,624,300,641,313,682,278,670,258" href="'.$base_url.'/grants-awards-by-state/NC/'.$year.'" shape="poly" title="North Carolina" />' .
          '<area alt="North Dakota" coords="293,63,288,115,380,120,373,67" href="'.$base_url.'/grants-awards-by-state/ND/'.$year.'" shape="poly" title="North Dakota" />' .
          '<area alt="Ohio" coords="545,234,567,238,572,244,596,203,592,178,563,187,553,184,538,186,542,229" href="'.$base_url.'/grants-awards-by-state/OH/'.$year.'" shape="poly" title="Ohio" />' .
          '<area alt="Oklahoma" coords="290,276,407,281,412,340,359,331,331,324,331,290,288,288" href="'.$base_url.'/grants-awards-by-state/OK/'.$year.'" shape="poly" title="Oklahoma" />' .
          '<area alt="Oregon" coords="50,66,22,121,22,135,113,157,118,114,131,98,125,87,83,87,61,81,60,69" href="'.$base_url.'/grants-awards-by-state/OR/'.$year.'" shape="poly" title="Oregon" />' .
          '<area alt="Pennsylvania" coords="591,175,597,213,660,200,671,192,665,186,667,171,658,159" href="'.$base_url.'/grants-awards-by-state/PA/'.$year.'" shape="poly" title="Pennsylvania" />' .
          '<area alt="Rhode Island" coords="708,149,713,156,707,163,702,153" href="'.$base_url.'/grants-awards-by-state/RI/'.$year.'" shape="poly" title="Rhode Island" />' .
          '<area alt="South Carolina" coords="572,303,616,349,638,314,625,301,597,297" href="'.$base_url.'/grants-awards-by-state/SC/'.$year.'" shape="poly" title="South Carolina" />' .
          '<area alt="South Dakota" coords="289,115,380,120,377,124,383,131,383,163,380,179,355,173,313,169,285,168,286,152,286,134" href="'.$base_url.'/grants-awards-by-state/SD/'.$year.'" shape="poly" title="South Dakota" />' .
          '<area alt="Tennessee" coords="475,312,557,305,588,273,480,287" href="'.$base_url.'/grants-awards-by-state/TN/'.$year.'" shape="poly" title="Tennessee" />' .
          '<area alt="Texas" coords="289,285,281,366,229,360,255,405,276,418,290,404,307,407,340,467,367,477,367,444,423,408,426,381,416,344,403,337,331,325,330,288" href="'.$base_url.'/grants-awards-by-state/TX/'.$year.'" shape="poly" title="Texas" />' .
          '<area alt="Utah" coords="152,168,190,174,187,194,214,195,204,268,135,256" href="'.$base_url.'/grants-awards-by-state/UT/'.$year.'" shape="poly" title="Utah" />' .
          '<area alt="Vermont" coords="670,103,681,140,688,140,691,100" href="'.$base_url.'/grants-awards-by-state/VT/'.$year.'" shape="poly" title="Vermont" />' .
          '<area alt="Virginia" coords="582,259,565,276,670,257,658,234,647,230,649,223,638,215,628,215,610,231,606,254" href="'.$base_url.'/grants-awards-by-state/VA/'.$year.'" shape="poly" title="Virginia" />' .
          '<area alt="Washington" coords="48,63,61,71,61,79,93,85,132,91,140,41,73,21,50,28" href="'.$base_url.'/grants-awards-by-state/WA/'.$year.'" shape="poly" title="Washington" />' .
          '<area alt="West Virginia" coords="637,217,628,216,620,232,611,230,607,248,598,257,584,260,575,248,579,229,592,219,594,201,597,214,612,212,612,219,628,211,635,212" href="'.$base_url.'/grants-awards-by-state/WV/'.$year.'" shape="poly" title="West Virginia" />' .
          '<area alt="Wisconsin" coords="436,108,434,117,428,122,428,141,449,154,450,175,458,180,494,175,501,130,495,128,488,119,457,105,449,100" href="'.$base_url.'/grants-awards-by-state/WI/'.$year.'" shape="poly" title="Wisconsin" />' .
          '<area alt="Wyoming" coords="199,123,186,194,281,204,286,131" href="'.$base_url.'/grants-awards-by-state/WY/'.$year.'" shape="poly" title="Wyoming" />' .
          '<area alt="Connecticut" coords="776,186,20" href="'.$base_url.'/grants-awards-by-state/CT/'.$year.'" shape="circle" title="Connecticut" />' .
          '<area alt="Delaware" coords="725,260,19" href="'.$base_url.'/grants-awards-by-state/DE/'.$year.'" shape="circle" title="Delaware" />' .
          '<area alt="Maryland" coords="726,300,20" href="'.$base_url.'/grants-awards-by-state/MD/'.$year.'" shape="circle" title="Maryland" />' .
          '<area alt="Massachusetts" coords="776,96,20" href="'.$base_url.'/grants-awards-by-state/MA/'.$year.'" shape="circle" title="Massachusetts" />' .
          '<area alt="New Hampshire" coords="676,50,20" href="'.$base_url.'/grants-awards-by-state/NH/'.$year.'" shape="circle" title="New Hampshire" />' .
          '<area alt="New Jersey" coords="726,218,20" href="'.$base_url.'/grants-awards-by-state/NJ/'.$year.'" shape="circle" title="New Jersey" />' .
          '<area alt="Rhode Island" coords="775,140,19" href="'.$base_url.'/grants-awards-by-state/RI/'.$year.'" shape="circle" title="Rhode Island" />' .
          '<area alt="Vermont" coords="642,81,20" href="'.$base_url.'/grants-awards-by-state/VT/'.$year.'" shape="circle" title="Vermont" />' .
          '<area alt="Washington, D.C." coords="725,342,19" href="'.$base_url.'/grants-awards-by-state/DC/'.$year.'" shape="circle" title="Washington, D.C." /></map></p>';
      
      return check_markup($MapString, 'full_html');
  }
}
