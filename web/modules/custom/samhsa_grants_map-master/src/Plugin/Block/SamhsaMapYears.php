<?php

namespace Drupal\samhsa_grants_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SamhsaMapYears' block.
 *
 * @Block(
 *  id = "samhsa_map_years",
 *  admin_label = @Translation("Samhsa map years"),
 * )
 */
class SamhsaMapYears extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
      return [ '#markup' => $this->_grants_years(), '#cache' => ['max-age' => 0,] ];
  }
  
  private function _grants_years(){
      global $base_url;
      
      $YearString = '<br><center>' .    
        '<h2>View Grant Awards By Fiscal Year: <a href="' . $base_url.
        '/grants-awards-state?year=2018">2018</a> | <a href="' . $base_url.
        '/grants-awards-state?year=2017">2017</a> | <a href="' . $base_url.
        '/grants-awards-state?year=2016">2016</a> | <a href="' . $base_url.
        '/grants-awards-state?year=2015">2015</a> | <a href="' . $base_url.
        '/grants-awards-state?year=2014">2014</a>' .
        '<br></h2></center><br>' .  
        '<p>This site provides information on grants issued by SAMHSA for mental' .
        'health and substance abuse services by State. The summaries include' .
        'Drug Free Communities grants issued by SAMHSA on behalf of the Office' .
        'of National Drug Control Policy.<br><br>' .
        'The dollar amounts for the grants should not be used for SAMHSA ' .
        'budgetary purposes.</p><br>';
      
      return $YearString;
  }
}
