<?php

namespace Drupal\samhsa_grants_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SamhsaMapTerritories' block.
 *
 * @Block(
 *  id = "samhsa_map_territories",
 *  admin_label = @Translation("Samhsa map territories"),
 * )
 */
class SamhsaMapTerritories extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        return [ '#markup' => $this->_grants_territories(), '#cache' => ['max-age' => 0,] ];
    }
  
  private function _grants_territories(){
      global $base_url;
      
      if (isset($_GET['year'])) {
          $year = $_GET['year'];
      }else{ $year = '2018'; }
      
      $ListString = '<h2>Pacific Islands:</h2>' .   
           '<ul>' .
	       '<li><a href="'.$base_url.'/grants-awards-by-state/AS/'.$year.'">American Samoa</a></li>' .
	       '<li><a href="'.$base_url.'/grants-awards-by-state/GU/'.$year.'">Guam</a></li>' .
	       '<li><a href="'.$base_url.'/grants-awards-by-state/FM/'.$year.'">Fed. Micronesia</a></li>' .
	       '<li><a href="'.$base_url.'/grants-awards-by-state/MH/'.$year.'">Marshall Islands</a></li>' .
	       '<li><a href="'.$base_url.'/grants-awards-by-state/MP/'.$year.'">N. Marianas Isle.</a></li>' .
	       '<li><a href="'.$base_url.'/grants-awards-by-state/PW/'.$year.'">Rep. Palau</a></li>' .
           '</ul>' .
           '<h2>Caribbean:</h2>' .   
           '<ul>' .
	       '<li><a href="'.$base_url.'/grants-awards-by-state/PR/'.$year.'">Puerto Rico</a></li>' .
	       '<li><a href="'.$base_url.'/grants-awards-by-state/VI/'.$year.'">Virgin Islands</a></li>' .
           '</ul>';
      
      return $ListString; 
  }
}
