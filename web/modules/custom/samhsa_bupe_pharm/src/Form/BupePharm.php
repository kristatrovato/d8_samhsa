<?php

namespace Drupal\samhsa_bupe_pharm\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Json;

/**
 * Class BupePharm.
 */
class BupePharm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bupe_pharm_lookup';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['header'] = [
          '#type' => 'markup',
          '#markup' => "<p>To verify a practitioner's DATA waiver, search using his or her last name " .
                       "and DEA registration number.</p>",
    ];
    $form['practitioner'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Practitioner Last Name'),
      '#maxlength' => 32,
      '#size' => 32,
      '#required' => true,
    ];
    $form['dea_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DEA Registration Number'),
      '#maxlength' => 32,
      '#size' => 32,
      '#required' => true,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => '1',
    ];
    $form['footer'] = [
        '#type' => 'markup',
        '#markup' => "<p>Each practitioner's DEA license gives two registration numbers. " . 
                     "<strong>Search using the first number</strong>, which generally starts with A, B, F or M.</p>" .
                     "<p>If you need to search for a number that starts with 'X' replace the 'X' with an asterisk (*).</p>",
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
      /*
      if (is_numeric($form_state['values']['dr_lastname'])) {
          drupal_set_message(t('Physician Last Name field must be Non-Numeric'), 'error');
      }
      */
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    $uri = "https://buprenorphine.samhsa.gov/api/pharmacist-lookup.php?dea_registration_nmbr=";
    $uri .= $form_state->getValue('dea_number') . "&name_last=" . rawurlencode($form_state->getValue('practitioner'));

    try {
        $response = \Drupal::httpClient()->get($uri, array('headers' => array('Accept' => 'text/plain')));
        $data = (string) $response->getBody();
        if (empty($data)) {
            return FALSE;
        }
    }
    catch (RequestException $e) {
        return FALSE;
    }
    
    $json_response = Json::decode($data);
     
    if (isset($json_response['data']['attributes']['name_last'])) {
        
        $msg_result = $json_response['data']['attributes']['name_first'] . ' ';
        $msg_result .= $json_response['data']['attributes']['name_last'] . ' is a certified Buprenorphine Physician. ';
        \Drupal::messenger()->addMessage($msg_result);
        $msg_result = 'DEA Registration Number: ' . $json_response['data']['attributes']['dea_registration_nmbr'];
        \Drupal::messenger()->addMessage($msg_result);
        $msg_result = 'Licensed State: ' . $json_response['data']['attributes']['license_state'];
        \Drupal::messenger()->addMessage($msg_result);
        $msg_result = 'Date Certified: ' . substr($json_response['data']['attributes']['certification_date'], 0, -9);
        \Drupal::messenger()->addMessage($msg_result);
        
        $msg_result = 'Certified for ' . $json_response['data']['attributes']['number_patients'] . ' patients.';
        \Drupal::messenger()->addMessage($msg_result);
        
    } elseif ($json_response['errors']['status'] == "404") {
        
        $msg_result = $form_state->getValue('practitioner') . ' is not a Buprenorphine Certified Physician.<br />';
        $msg_result .= 'DEA Registration Number: ' . $form_state->getValue('dea_number');
        \Drupal::messenger()->addMessage(t($msg_result), 'error');
        
    } else {
        
        $msg_result = 'The following information is not available at this time. Please try again later.<br />';
        \Drupal::messenger()->addMessage(t($msg_result), 'error');
        
    }
  /*  
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      \Drupal::messenger()->addMessage($key . ': ' . ($key === 'text_format'?$value['value']:$value));
    }
  */
  } // end method
}   // end class
