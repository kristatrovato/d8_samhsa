(function (Drupal, $, window) {
  Drupal.behaviors.omega_samhsa_main = {
    attach: function (context, settings) {

      // Execute code once the DOM is ready.
      //----------------------------------------------------------------------------------------------
      
      
      
      // Execute code once the window is fully loaded.
      //----------------------------------------------------------------------------------------------
      $(window).on('load', function () {

      });


      // Execute code when the window is resized.
      //----------------------------------------------------------------------------------------------
      $(window).resize(function () {
        
      });


      // Execute code when the window scrolls.
      //----------------------------------------------------------------------------------------------
      $(window).scroll(function () {

      });

    }
  };
//Open EBP Filters on mobile
  
  Drupal.behaviors.ebpFilterAccordion = {
		  attach: function (context, settings) {
		    $('.view-ebp-all-resources .view-header').once('ebpFilterAccordion').on('click', function(event){
		    	event.preventDefault();
		    	// create accordion variables
		    	var accordion = $(this);
		    	var accordionContent = accordion.next('.view-filters');
		    	var accordionToggleIcon = $('.accordion-toggle').children('.toggle-icon');
		    	
		    	// toggle accordion link open class
		    	accordion.toggleClass("open");
		    	// toggle accordion content
		    	accordionContent.slideToggle(250);
		    	
		    	// change plus/minus icon
		    	if (accordion.hasClass("open")) {
		    		accordionToggleIcon.html("<i class='fas fa-minus-circle'></i>");
		    	} else {
		    		accordionToggleIcon.html("<i class='fas fa-plus-circle'></i>");
		    	}
		    });
		  }
		};

  //Responsive image maps
  Drupal.behaviors.imageMaps = {
		  attach: function (context, settings) {
			  $('img[usemap]').once('imageMaps').rwdImageMaps();
		  }
  };

} (Drupal, jQuery, this));

