(function (Drupal, $, window) {
  Drupal.behaviors.omega_samhsa_grandchild_template = {
    attach: function (context, settings) {

      // Execute code once the DOM is ready.
      //----------------------------------------------------------------------------------------------
      
      
      
      // Execute code once the window is fully loaded.
      //----------------------------------------------------------------------------------------------
      $(window).on('load', function () {

      });


      // Execute code when the window is resized.
      //----------------------------------------------------------------------------------------------
      $(window).resize(function () {
        
      });


      // Execute code when the window scrolls.
      //----------------------------------------------------------------------------------------------
      $(window).scroll(function () {

      });

    }
  };

} (Drupal, jQuery, this));
